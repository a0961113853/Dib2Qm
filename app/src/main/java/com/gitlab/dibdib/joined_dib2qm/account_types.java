// Copyright (C) 2016 Roland Horsch and others:
// -- Changes:  Copyright (C) 2016,2017  Roland Horsch <gx work s{at}mai l.de>.
// -- Original: Copyright (C) 2014/2015  Jeroen Vreeken.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// This part is based on the corresponding code from net.vreeken.quickmsg.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package com.gitlab.dibdib.joined_dib2qm;

import java.util.*;

public class account_types { // extend  net.vreeken.quickmsg.account_types
//=====

public static account_type generic = new account_type(
	"GMail, GMX, mail.de etc.", "foo.bar@gmail.com",
	null,
	"imap.gmail.com", "993",
	"smtp.gmail.com", "587");

public static account_type mail_de = new account_type(
	"MAIL.DE", "foobar@mail.de",
	null,
	"imap.mail.de", "993",
	"smtp.mail.de", "587");

public static account_type web_de = new account_type(
	"WEB.DE", "foobar@web.de",
	null,
	"imap.web.de", "993",
	"smtp.web.de", "587");

public static account_type gmx = new account_type(
	"GMX", "foobar@gmx.de",
	null,
	"imap.gmx.com", "993",
	"mail.gmx.com", "587");

public static account_type mykolab = new account_type(
	"MyKolab", "foobar@mykolab.com",
	null,
	"imap.mykolab.com", "993",
	"smtp.mykolab.com", "587");

public static account_type riseup = new account_type(
	"Riseup", "foobar@riseup.net",
	"@riseup.net",
	"mail.riseup.net", "143",
	"mail.riseup.net", "465");

public static List<account_type> account_types =
	new ArrayList<account_type>( Arrays.asList(
		generic));

//=====
}
