// Copyright (C) 2016,2017,2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// This part is new code for working with QuickMSG, based on the
// corresponding API from net.vreeken.quickmsg.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package com.gitlab.dibdib.joined_dib2qm;

import com.gitlab.dibdib.dib2qm.ContextIf_OLD;

public interface PrivProvIf {
//=====

public void init( ContextIf_OLD app_context);

public attachment pgpmime_id();

public attachment encrypt_sign( attachment unenc, String to);

public attachment key_attachment( String memberAddr);

public boolean load_keys();

/**
 * @param xInputStream
/** @param xyKeyFound
/** @return Fallback key value, null if error or if value has not changed
 */
public byte[] public_keyring_add_key( byte[] xInputStream, String[] xyKeyFound);

public attachment decrypt_verify( attachment attachment);

public String fingerprint( String addr);

public void public_keyring_remove_by_address( String addr);

//=====
}
