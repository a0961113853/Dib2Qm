package net.sf.dibdib.thread_wk;

import java.util.*;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.Feed.Script;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_ui.UiPres;
import net.sf.dibdib.thread_x.*;

/** 'View model' provider, enumeration as owner referencing actual implementation.
 */
public enum FeederRf {
//=====

DISCLAIMER( "NN", "LC", "LICENSE"),
INTRO( "IN"),
HELP( "HP"),
ABOUT( "AB"),
LOGIN( "LG"),
CALC( "CC", "AWT", "CO", "TTY"),
//CALEND("CN"),
//,
;

private FeederIf mFeeder;
private final String[] mOptionalNames;

private FeederRf( String... optionalNames) {
	mFeeder = null;
	mOptionalNames = optionalNames;
}

/** Get the actual implementation.
 * @return implementer.
 */
public FeederIf get() {
	if (null != mFeeder) {
		return mFeeder;
	}
	switch (this) {
	case DISCLAIMER:
		mFeeder = new GenericTextFeeder( this);
		break;
	case INTRO:
		mFeeder = new IntroFeeder( this);
		break;
	case HELP:
	case ABOUT:
		mFeeder = new HelpFeeder( this);
		break;
	case LOGIN:
		mFeeder = new LoginFeeder( this);
		break;
	case CALC:
		mFeeder = new CalcFeeder( this);
		break;
	default:
		return null;
	}
	mFeeder.onStart();
	return mFeeder;
}

public String getShortId2() {
	return mOptionalNames[0];
}

public static FeederRf find( FeederIf impl) {
	for (FeederRf fd : FeederRf.values()) {
		if (impl.getClass().getName().toUpperCase( Locale.ROOT).contains( fd.name())) {
			return fd;
		}
	}
	return DISCLAIMER;
}

//=====
public static interface FeederIf {
//=====

public FeederRf onStart();

/** Get initial/ next/ previous feeder.
 * @param handle 0=initial, 1=next
 * @return feeder
 */
public FeederRf switchFeeder( int handle, String... optionalName);

public FeederIf setFeeder( int handle, FeederRf next);

public int getSlideEnd();

public int getSlideStart();

public int findSlideSupplement( int slideFrom, int count);

/** To be called on worker thread: atomically sets the value for getLastFeed().
 * @param ttyMode if: requires special treatment for TTY style.
 * @param param Proprietary parameters.
 * @return The script.
 */
public Feed.Script prepareFeed( int slide, int supplement, boolean ttyMode, String... param);

public Feed.Script getLastFeed();

public QValToken filter( QValToken out);

//=====
}

//=====
public static class GenericFeeder implements FeederIf {
//=====

protected final FeederRf me;
protected FeederRf nextFeeder;
protected volatile Feed.Script mFeed = null;

public GenericFeeder( FeederRf owner) {
	me = owner;
	nextFeeder = me;
}

protected int linesPerSlide() {
	return -1 + UiDataSto.UI_BOARD_HEIGHT.i32( null) / UiDataSto.UI_LINE_SPACING_PT10.i32( null);
}

@Override
public FeederRf onStart() {
	UiDataSto.UI_DISPLAY_SPLIT_CANVAS_X.prep( Dib2Constants.UI_DSPL_INIT_SPLIT_X, null);
	return me;
}

public FeederRf getOwner() {
	return me;
}

@Override
public FeederRf switchFeeder( int handle, String... optionalName) {
	if (0 < handle) {
		if (me != nextFeeder) {
			Dib2Root.ui.bDisclaimerDone = true;
		}
		nextFeeder.get().onStart();
	}
	return (0 < handle) ? nextFeeder : me;
}

@Override
public FeederIf setFeeder( int handle, FeederRf next) {
	nextFeeder = next;
	return this;
}

@Override
public Script prepareFeed( int slide, int supplement, boolean terminalMode, String... param) {
	// To be implemented.
	mFeed = null;
	return mFeed;
}

@Override
public Script getLastFeed() {
	return mFeed;
}

@Override
public QValToken filter( QValToken cmd) {
	return cmd;
}

@Override
public int getSlideEnd() {
	return 2;
}

@Override
public int getSlideStart() {
	return 1;
}

@Override
public int findSlideSupplement( int slideFrom, int count) {
	return 1;
}

//=====
}

private static String[] GenericTextFeeder_license = null;

//=====
public static class GenericTextFeeder extends GenericFeeder {
//=====

public GenericTextFeeder( FeederRf owner) {
	super( owner);
}

protected String[] zFeedTxt = Dib2Lang.kUiAgree.clone();
protected int zFeedLinesSkip = 0;

@Override
public FeederRf onStart() {
	// In case the language has changed:
	GenericTextFeeder_license = null;
	UiDataSto.UI_DISPLAY_SPLIT_CANVAS_X.prep(
		Dib2Root.ui.bDisclaimerDone ? 0 : Dib2Constants.UI_DSPL_INIT_SPLIT_X,
		null);
	UiDataSto.UI_ZOOMLVL_CANVAS.prep( -1, null);
	return me;
}

protected int prepareTextLines( int slide, int supplement, boolean terminalMode) {
	final int lines = linesPerSlide();
	if (100 > zFeedTxt.length) {
		zFeedTxt = new String[(100 > lines) ? 100 : lines];
	}
	///// Disclaimer and license as default
	String[] out = Dib2Lang.kUiAgree;
	if (Dib2Root.ui.bDisclaimerDone) {
		if (null == GenericTextFeeder_license) {
			GenericTextFeeder_license = Dib2Root.platform.getLicense( Dib2Lang.kLicensePre[Dib2Root.ui.iLang]);
		}
		out = GenericTextFeeder_license;
	}
	int start = (slide - 1) * lines;
	start = (out.length <= start) ? (out.length - 1) : start;
	int end = start + lines;
	end = (out.length < end) ? out.length : end;
	System.arraycopy( out, start, zFeedTxt, 0, end - start);
	zFeedTxt[out.length] = null;
	return out.length;
}

@Override
public Feed.Script prepareFeed( int slide, int supplement, boolean terminalMode, String... param) {
	int c0 = prepareTextLines( slide, supplement, false);
	String[] lines = zFeedTxt;
	int no = 0;
	int count = 0;
	Feed.Script[] script = new Feed.Script[2 + 6 * c0];
//	int margin = Dib2Constants.UI_MARGIN;
	final int linesp = UiDataSto.UI_LINE_SPACING_PT10.i32( null);
	final int height = UiDataSto.UI_FONT_SIZE_PT10.i32( null);
	final int base = linesp - ((height
		* Dib2Constants.UI_FONT_NMZ_DESCENT >> Dib2Constants.UI_FONT_NMZ_SHIFT));
	script[count++] = Feed.makeScriptEl( ++no, Feed.RBASE, base);
	for (int i0 = 0; i0 < c0; i0 += 1 + zFeedLinesSkip) {
		String[] tabbed = lines[i0].split( "\t");
		if ((count + tabbed.length * 2 + 2) >= script.length) {
			script = Arrays.copyOf( script, 2 * (script.length + tabbed.length));
		}
		int posx = 0; //Dib2Constants.UI_MARGIN;
		for (String s0 : tabbed) {
			script[count++] = Feed.makeScriptEl( ++no, Feed.POSX, posx);
			if ((0 >= i0) && lines[i0].startsWith( "\t@") && s0.startsWith( "@")) {
				script[count++] = Feed.makeScriptEl( ++no, Feed.ENTRY);
			} else {
				script[count++] = Feed.makeScriptEl( ++no, Feed.TEXT, s0);
			}
			posx += UiFunc.boundWidthNmz( s0 + 'm', height);
			posx = (((posx - 2) / Dib2Constants.UI_DSPL_NMZ_TAB) + 1)
				* Dib2Constants.UI_DSPL_NMZ_TAB;
		}
		script[count++] = Feed.makeScriptEl( ++no, Feed.TXLF);
	}
	Feed.Script out = Feed.makeScript( 0);
	out.script = script;
	out.cScript = count;
	// Atomic:
	mFeed = out;
	return out;
}

@Override
public Script getLastFeed() {
	if (null == mFeed) {
		mFeed = prepareFeed( 1, 0, false);
	}
	return mFeed;
}

@Override
public QValToken filter( QValToken cmd) {
	///// For disclaimer and license.
	switch (cmd.operator) {
	case VIEW:
		///// Switch to license or next feeder.
		UiDataSto.UI_DISPLAY_SPLIT_CANVAS_X.prep( 0, null);
		Dib2Root.ui.feederCurrent = me;
		if (Dib2Root.ui.bDisclaimerDone) {
			nextFeeder.get().onStart();
			Dib2Root.ui.feederCurrent = nextFeeder;
		}
		mFeed = null;
		Dib2Root.ui.bDisclaimerDone = true;
		return null;
	case ESCAPE:
		if (!Dib2Root.ui.bDisclaimerDone) {
			MainThreads.wxExitRequest = true;
			break;
		}
		// Fall through
	case STEP:
		Dib2Root.ui.bDisclaimerDone = true;
		Dib2Root.ui.feederMain.get().onStart();
		Dib2Root.ui.feederCurrent = Dib2Root.ui.feederMain;
		return null;
	case NOP:
	case DUP:
		Dib2Root.ui.bDisclaimerDone = true;
		nextFeeder.get().onStart();
		Dib2Root.ui.feederCurrent = nextFeeder;
		return null;
	default:
		;
	}
	return null;
}

//=====
}

//=====
static class IntroFeeder extends GenericTextFeeder {
//=====

public IntroFeeder( FeederRf owner) {
	super( owner);
}

@Override
protected int prepareTextLines( int slide, int supplement, boolean terminalMode) {
	zFeedTxt[0] = "Intro not ready yet.";
	zFeedTxt[1] = "Press green '>' to continue.";
	zFeedTxt[2] = null;
	return 2;
}

@Override
public QValToken filter( QValToken cmd) {
	switch (cmd.operator) {
	case VIEW:
	case ESCAPE:
	case STEP:
	case NOP:
	case DUP:
		Dib2Root.ui.feederMain.get().onStart();
		Dib2Root.ui.feederCurrent = Dib2Root.ui.feederMain;
		break;
	default:
		;
	}
	return null;
}
//=====
}

//=====
static class HelpFeeder extends GenericTextFeeder {
//=====

public HelpFeeder( FeederRf owner) {
	super( owner);
}

static private String[] getHelp_lines = null;

static String[] getHelp( boolean terminalMode) {
	if (null == getHelp_lines) {
		int count = 0;
		String[] lines = new String[3 + QCalc.values().length + 1];
		lines[0] = "Version " + Dib2Constants.VERSION_STRING + ". " + Dib2Constants.NO_WARRANTY[0];
		lines[1] = "List of available FUNCTIONS (see below, e.g.:"
			+ (terminalMode ? " type '\\' + file name, press ENTER, ';EXPORT', ENTER):"
				: " ^FileName, ^ENTER, 'EXPORT', GO):");
		lines[2] = "(Not fully implemented yet !)";
		count = 3;
		for (QCalc funct : QCalc.values()) {
			String descr = funct.getDescription();
			if ('.' != descr.charAt( 0)) {
				lines[count++] = descr;
			}
		}
		lines[count++] = terminalMode
			? "(Use with preceding ';' for commands, '\\' for data."
			: "(E.g. type '3', press > or ENTER, '4', > or ENTER,";
		lines[count++] = terminalMode
			? "E.g.: press '\\', type file name, press ENTER, type ';EXPORT', press ENTER)"
			: "type 'ADD', (press > or ENTER,) press GO)";
		getHelp_lines = Arrays.copyOf( lines, count);
	}
	return getHelp_lines;
}

@Override
protected int prepareTextLines( int slide, int supplement, boolean terminalMode) {
	zFeedTxt = getHelp( terminalMode);
	return zFeedTxt.length;
}

//=====
}

//=====
static class LoginFeeder extends GenericTextFeeder {
//=====

private String mPathDataFile = null;

public LoginFeeder( FeederRf owner) {
	super( owner);
}

public void setPath( String pathDataFile) {
	mPathDataFile = pathDataFile;
}

@Override
protected int prepareTextLines( int slide, int supplement, boolean terminalMode) {
	zFeedTxt = TcvCodec.instance.setAccessCode( null) ? Dib2Lang.kUiStepPw : Dib2Lang.kUiStepAc;
	return zFeedTxt.length;
}

@Override
public QValToken filter( QValToken cmd) {
	String entry = cmd.uiParameter;
	switch (cmd.operator) {
	case ESCAPE:
		if (!TcvCodec.instance.setAccessCode( null)
			&& "0".equals( UiPres.INSTANCE.getEntry( true))) {
			TcvCodec.instance.setDummyPhrase( true);
			Dib2Root.ui.feederMain.get().onStart();
			Dib2Root.ui.feederCurrent = Dib2Root.ui.feederMain;
			return null;
		}
		// Clear it:
		TcvCodec.instance.setAccessCode( new byte[0]);
		return null;
	case STEP:
	case NOP:
		if (CcmSto.instance.isInitialized() || (null == mPathDataFile)) {
			Dib2Root.ui.feederMain.get().onStart();
			Dib2Root.ui.feederCurrent = Dib2Root.ui.feederMain;
			break;
		}
		entry = UiPres.INSTANCE.getEntry( true);
		entry = (0 >= entry.length()) ? cmd.uiParameter : entry;
		// Check AC:
		if (TcvCodec.instance.setAccessCode( null)) {
			TcvCodec.instance.setHexPhrase( StringFunc.hexUtf8( entry, false));
			MainThreads.INSTANCE.triggerSaveOrInitialLoad();
//				if (CcmSto.instance.isInitialized()) {//StoRunner.INSTANCE.loadData( mPathDataFile)) {
//					mPathDataFile = null;
//				}
		} else {
			TcvCodec.instance.setAccessCode( StringFunc.bytesUtf8( entry));
		}
		break;
	default:
		;
	}
	return null;
}

//=====
}

public static FeederRf findFeeder( String name) {
	name = name.toUpperCase( Locale.ROOT);
	FeederRf feeder;
	try {
		feeder = FeederRf.valueOf( name);
	} catch (Exception e) {
		feeder = null;
	}
	if (null == feeder) {
		for (FeederRf fx : FeederRf.values()) {
			for (String opt : fx.mOptionalNames) {
				if (opt.equals( name)) {
					feeder = fx;
					break;
				}
			}
		}
		if (null == feeder) {
			return null;
		}
	}
	return feeder;
}

//=====
}
