// Copyright (C) 2018,2019,2021  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_wk;

import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.QValFunc.QVal;
import net.sf.dibdib.thread_x.*;

/** For platform's worker thread (CCM's QNet). */
public enum WkRunner implements QExec.QRunnerIf {
//=====

INSTANCE;

///// Threaded (in/ out/ trigger)

// Single Place for simplifying ...
public final QPlace wxGateIn = new QPlace(); // zSourceDsp.wxSource;
private QPlace rGateOut;

public static volatile Thread qActive = null;
public static volatile boolean qBusy = false;

/////

QValToken qPending = null;

ExecSlowPrcs qProcessSlow = null;
ExecFastPrcs qProcessFast = null;
TaskifyPrcs qTaskify = null;

public boolean init( QPlace... xrOutgoing) {
//	rGateOut = xrOutgoing[0];
	qProcessFast = new ExecFastPrcs();
	qProcessSlow = new ExecSlowPrcs();
	qTaskify = new TaskifyPrcs();
	return true;
}

@Override
public boolean requested() {
	return ((null != qActive) && qBusy) || (null != wxGateIn.peek()); //zSourceDsp.peek());
}

public void addSamples() {
	CcmSto.instance.stackPush( QValMapSto.qval4DoubleD4( 3.0 * 1.0e4));
	CcmSto.instance.stackPush( QValMapSto.qval4DoubleD4( 2.0 * 1.0e4));
}

public void pushResult( QVal[][] vals, int pos) {
	if ((null == vals) || (0 >= vals.length) || (null == vals[0])) {
		return;
	}
	CcmSto.instance.stackInsert( pos, vals);
}

@Override
public int prepare( long... parameters) {
	qActive = Thread.currentThread();
	JResult.getThreadIndex();
	return 0;
}

@Override
public int step() {
	try {
		QValToken task = wxGateIn.pull();
		if (null == task) {
			return 0;
		}
		QValToken result = qTaskify.exec( task);
		if (null != result) {
			wxGateIn.push( result);
		} else if (null == task.operator) {
			rGateOut.push( task);
		} else { //if (0 >= task.cArgsZipped) {
			result = qProcessFast.exec( task);
			if (null == result) {
				// push to 'SLOW' ...
				rGateOut.push( QValToken.EMPTY);
			} else if ((0 >= result.cArgsZippedTodo) && (qPending != result)) {
				rGateOut.push( result);
			} else {
				wxGateIn.push( result);
			}
		}
	} catch (Exception e) {
	}
	return (null == wxGateIn.peek()) ? 0 : 1;
}

//=====
}
