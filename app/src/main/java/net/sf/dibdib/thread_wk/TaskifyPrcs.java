// Copyright (C) 2019,2021  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_wk;

import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.QCalc;
import net.sf.dibdib.thread_any.QValFunc.QVal;
import net.sf.dibdib.thread_x.*;

public class TaskifyPrcs implements QExec.QPrcsHelperIf {
//=====

private QVal zLastX;

private QValToken checkStack( QValToken cmp) {
	for (int pos = 0; true; ++pos) {
		QValToken task = CcmSto.instance.stackPeek( pos);
		if (null == task) {
			break;
		}
		if ((null != task.args4Lazy) && (task != cmp)) {
			return task;
		}
	}
	return null;
}

@Override
public // 
QValToken exec( QValToken... tasks) {
	if (1 != tasks.length) {
		return null;
	}
	QValToken cmd = tasks[0];
	if (null == cmd) {
		return null;
	} else if (WkRunner.INSTANCE.qPending == cmd) {
		if (null == cmd.args4Lazy) {
			WkRunner.INSTANCE.qPending = checkStack( cmd);
			return WkRunner.INSTANCE.qPending;
		}
		return null;
	} else if (null != cmd.args4Lazy) {
		WkRunner.INSTANCE.qPending = cmd;
		return cmd;
	} else if ((null == cmd.operator) || (null != cmd.val)) {
		return null;
	}
	final JResult pooled = JResult.get8Pool();
	zLastX = CcmSto.instance.variable_get( "L");
	if ((null != cmd.uiParameter) && (0 < cmd.uiParameter.length())) {
		CcmSto.instance.stackPush( QValMapSto.qvalAtoms4String( pooled, cmd.uiParameter));
	}
	if (QCalc.RCX == cmd.operator) {
		final String str = QValMapSto.string4QVal( ((null == zLastX) ? QValMapSto.NIL : zLastX));
		final QVal[] result = QValMapSto.qvalAtoms4String( pooled, str);
		CcmSto.instance.stackPush( result);
		return QValToken.create( result);
	}
	int cArgs = cmd.operator.cArgs;
	if (0 > cArgs) {
		CcmSto.instance.stackPush( QValMapSto.qval4AtomicLiteral( pooled, "(not implemented yet)"));
		return QValToken.EMPTY;
	}
	cmd.args4Lazy = CcmSto.instance.stackPop( cArgs, cmd.operator);
	cmd.cArgsZippedTodo = 0;
	if ((QCalc.NOP == cmd.operator) || (null == cmd.args4Lazy) || (cArgs > cmd.args4Lazy.length)) {
		return QValToken.EMPTY;
	}
	if (cmd.operator == QCalc.STEP) {
		// TODO: for [...] take whole stack
		cmd.operator = QCalc.getOperator( QValMapSto.string4Literals( cmd.args4Lazy[0].val));
		cmd.operator = (null == cmd.operator) ? QCalc.NOP : cmd.operator;
		cArgs = cmd.operator.cArgs;
		cmd.args4Lazy = CcmSto.instance.stackPop( cArgs, cmd.operator);
		if (null == cmd.args4Lazy) {
			return QValToken.EMPTY;
		}
	}

	if (0 < cmd.operator.cReturnValues) {
		CcmSto.instance.stackPush( cmd);
		if (1 < cmd.operator.cReturnValues) {
			cmd.result1 = new QValToken();
			CcmSto.instance.stackPush( cmd.result1);
		}
		if (2 < cmd.operator.cReturnValues) {
			cmd.result2 = new QValToken();
			CcmSto.instance.stackPush( cmd.result2);
		}
	}
	return cmd;
}

//=====
}
