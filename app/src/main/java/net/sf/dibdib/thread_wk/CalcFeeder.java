// Copyright (C) 2018,2019,2020 Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_wk;

import static net.sf.dibdib.thread_wk.FeederRf.*;

import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_wk.FeederRf.*;
import net.sf.dibdib.thread_x.*;

final class CalcFeeder extends GenericTextFeeder {
//=====

public CalcFeeder( FeederRf owner) {
	super( owner);
}

@Override
public FeederRf onStart() {
	UiDataSto.UI_DISPLAY_SPLIT_CANVAS_X.prep( Dib2Constants.UI_DSPL_INIT_SPLIT_X, null);
	UiDataSto.UI_ZOOMLVL_CANVAS.prep( 0, null);
	return me;
}

@Override
protected int prepareTextLines( int slide, int supplement, boolean terminalMode) {
	long xbHexMemory = 3;
	int xOffset = 1;
	boolean partial = false;
	zFeedTxt = (null == zFeedTxt) ? new String[30] : zFeedTxt;
	int len = CcmSto.instance.stackRead( xbHexMemory, zFeedTxt, xOffset, partial);
	// Need some 10 lines for possible memory values.
	if ((0 <= len) && !partial && ((len + 10) >= zFeedTxt.length)) {
		len = -len / 2 - 10 / 2 - 2;
	}
	if (0 > len) {
		zFeedTxt = new String[-len * 2 + 2];
		len = CcmSto.instance.stackRead( xbHexMemory, zFeedTxt, xOffset, true);
	}
	for (int i0 = zFeedTxt.length - 1; i0 >= len; --i0) {
		zFeedTxt[i0] = null;
	}
	zFeedTxt[0] = "" + '\t' + '@'; //"\t" + StringFunc.string4Mnemonics( UiPres.INSTANCE.getEntry());
	for (int i0 = len - 1; i0 >= 0; --i0) {
		zFeedTxt[i0] = StringFunc.makePrintable( zFeedTxt[i0]);
	}
	return len;
}

@Override
public FeederRf switchFeeder( int handle, String... optionalName) {
	Dib2Root.ui.bDisclaimerDone = true;
	if (!CcmSto.instance.isInitialized()) {
		String path = StoRunner.check4Load();
		///// Try with dummy phrase -- maybe good enough for simple calculator etc.
		if (TcvCodec.instance.setDummyPhrase( false)) {
			if (null == path) {
				Dib2Root.ui.bDisclaimerDone = false;
				//WkRunner.INSTANCE.addSamples();
				// Mark as initialized:
				CcmSto.instance.stackPush();
				INTRO.get().setFeeder( 1, me);
				DISCLAIMER.get().setFeeder( 1, INTRO);
				return Dib2Root.ui.bDisclaimerDone ? me : DISCLAIMER;
			} else {
				MainThreads.INSTANCE.triggerSaveOrInitialLoad();
			}
			if (0 >= StoRunner.qLastSave) {
				return me;
			}
		}
		LOGIN.get().setFeeder( 1, me);
		((LoginFeeder) LOGIN.get()).setPath( path);
		return LOGIN;
	}
	if (0 == handle) {
		return me;
	} else if (me == nextFeeder) {
		if (0 < optionalName.length) {
			final FeederRf feeder = FeederRf.findFeeder( optionalName[0]);
			if ((null != feeder) && ((feeder.get() instanceof FeederRf.GenericTextFeeder)
				|| (feeder.get() instanceof FeederRf.HelpFeeder))) {
				feeder.get().setFeeder( 1, me);
				return feeder;
			}
		}
		final FeederRf flc = FeederRf.findFeeder( "LC");
		flc.get().setFeeder( 1, me);
		FeederRf.HELP.get().setFeeder( 1, flc);
		return FeederRf.HELP;
	}
	nextFeeder.get().onStart();
	return nextFeeder;
}

@Override
public QValToken filter( QValToken cmd) {
	return cmd;
}
//=====
}
