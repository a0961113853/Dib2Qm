// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_wk;

import static net.sf.dibdib.thread_any.QCalc.PW;

import java.io.File;
import java.util.*;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_any.QValFunc.QVal;
import net.sf.dibdib.thread_x.*;

final class ExecFastPrcs implements QExec.QPrcsHelperIf {
//=====

///// Threaded (in/ out/ trigger)

//final QPlace wxCommands = new QPlace();
//private QPlace rResults;

/////

private QVal[] fastExec( QCalc op, QVal[][] args) {
	final JResult pooled = JResult.get8Pool();
	QVal[] result = new QVal[1];
	try {
		Mapping mpg = null;
		String str;
		String str2;
		int len = 0;
		double num;
		double num2;
		switch (op) {

		case ABOUT:
			Dib2Root.ui.feederCurrent = FeederRf.ABOUT.get().onStart();
			return null;
		case EXIT:
			MainThreads.wxExitRequest = true; //...
			// feedProgress( new String[] { "Program is about to stop ... (Please press ENTER)." } );
			MainThreads.INSTANCE.triggerSaveOrInitialLoad();
			return null;
		case HELP:
			Dib2Root.ui.feederCurrent = FeederRf.HELP.get().onStart();
			return null;
		case PW:
		case PWAC:
			str = QValMapSto.string4Literals( args[0]);
			if ((null == str) || (0 >= str.length())) {
				return null;
			}
			if (PW == op) {
				TcvCodec.instance.setHexPhrase( StringFunc.hexUtf8( str, false));
			} else {
				TcvCodec.instance.setAccessCode( str.getBytes( StringFunc.STRX16U8));
			}
//			CcmSto.instance.stackPop( 1, null);
			result[0] = QValMapSto.TRUE;
			break;
		case QFILTER: {
			int flags = UiDataSto.UI_FILTER_CATS.i32( null) << 1;
			if (Mapping.Cats.OTHERS.flag >= flags) {
				flags = (int) Mapping.Cats.OTHERS.flag;
			} else if (Mapping.Cats.OTHERS.flag == (flags >>> 1)) {
				flags = 1 << 16;
			} else if (Mapping.Cats._MAX.flag == flags) {
				flags = (1 << 31) - 1;
			} else if (Mapping.Cats._MAX.flag < flags) {
				flags = (int) Mapping.Cats.OTHERS.flag;
			}
			UiDataSto//.qSwitches[UiDataSto.SWI_MAPPING_CATS] = flags;
				.UI_FILTER_CATS.prep( flags, UiDataSto.MAIN);
			Dib2Root.ui.feederCurrent = FeederRf.findFeeder( "LC").get().onStart();
		}
			return null;
		case UICOD:
			str = QValMapSto.string4Literals( args[0]);
			if ((null == str) || (0 >= str.length())) {
				return null;
			}
			UiDataSto.setUnicodeBlockOffset( str);
//			CcmSto.instance.stackPop( 1, null);
			return null;

		case MC:
			str = QValMapSto.string4QVals( args[0]);
			if ((null != args) && (null != str) && (0 < str.length())) {
				if (1 == str.length()) {
					str = "M" + str.charAt( 0);
				}
				CcmSto.instance.variable_set( str, null);
			}
			return null;

		case CLRALL:
			CcmSto.instance.stackClear( true);
			return null;
		case CLR1:
		case CLEAR:
			return null;
		case CLR2:
			return null;
		case CLR3:
			return null;
		case DATE:
			long date = MiscFunc.slotSecond16oDateApprox( MiscFunc.date4Millis( true));
			result[0] = QValMapSto.qval4SlotSecond16( date);
			break;
		case LANG:
			if ((0 >= args[0].length) || QValMapSto.isNumeric( args[0][0])) {
				Dib2Root.ui.iLang = (Dib2Root.ui.iLang + 1) % Dib2Lang.kLanguages.length;
			} else {
				str = QValMapSto.string4QVals( args[0]).toUpperCase( Locale.ROOT);
				for (int i0 = Dib2Lang.kLanguages.length - 1; i0 >= 0; --i0) {
					if (str.startsWith( Dib2Lang.kLanguages[i0])) {
						Dib2Root.ui.iLang = i0;
					}
				}
			}
			return null;
		case LOAD:
			str = QValMapSto.string4Literals( args[0]);
			if (1 == str.length()) {
				final char ch = str.charAt( 0);
				switch (ch) {
				case '!':
					str = "T";
					break;
				default:
					str = "M" + ch;
				}
			}
			str = QValMapSto.string4QVal( CcmSto.instance.variable_get( str));
			result = QValMapSto.qvalAtoms4String( pooled, str);
			break;
		case MMLD:
			len = args[0].length;
			result = (1 == len) ? result : new QVal[args[0].length];
			for (int i0 = 0; i0 < len; ++i0) {
				str = QValMapSto.string4QVal( args[0][i0]);
				if (1 == str.length()) {
					final char ch = str.charAt( 0);
					switch (ch) {
					case '!':
						str = "T";
						break;
					default:
						str = "M" + ch;
					}
				}
				result[i0] = CcmSto.instance.variable_get( str);
			}
			break;
		case MMSTO:
			len = args[0].length;
			result = (1 == len) ? result : new QVal[args[0].length];
			for (int i0 = 0; i0 < len; ++i0) {
				str = QValMapSto.string4QVal( args[1][i0 % args[1].length]);
				result[i0] = QValMapSto.NaN;
				if ((null != args) && (null != str) && (0 < str.length())) {
					if (1 == str.length()) {
						str = "M" + str.charAt( 0);
					}
					CcmSto.instance.variable_set( str, args[0][i0]);
					result = args[0];
				}
			}
			break;
		case NOP:
			break;
		case PRODUCT:
			num2 = 1.0e4;
			for (QVal v0 : args[0]) {
				num = QValMapSto.doubleD4oQVal( v0);
				if (!Double.isNaN( num)) {
					num2 = num2 * num / 1.0e8;
				}
			}
			result[0] = QValMapSto.qval4DoubleD4( num2);
			break;
		case QCAT:
			len = args[0].length;
			result = (1 == len) ? result : new QVal[args[0].length];
			for (int i0 = 0; i0 < len; ++i0) {
				result[i0] = QValMapSto.NaN;
				mpg = CcmSto.search4Oid( args[0][i0]);
				if (null != mpg) {
					if (0 != CcmSto.instance.update( mpg.clone4Cats( args[1][i0]))) {
						result[i0] = args[1][i0];
					}
				}
			}
			break;
		case QDEL:
			len = args[0].length;
			result = (1 == len) ? result : new QVal[args[0].length];
			for (int i0 = 0; i0 < len; ++i0) {
				result[i0] = CcmSto.instance.remove( args[0][i0], true) ? args[0][i0] : QValMapSto.NaN;
			}
			break;
		case QLOAD: {
			result[0] = QValMapSto.NaN;
			String oid = QValMapSto.string4QVals( args[0]);
			mpg = CcmSto.search4Oid( QValMapSto.qval4AtomicLiteral( pooled, oid));
		}
			break;
		case QOID: {
			result[0] = QValMapSto.NaN;
			String label = QValMapSto.string4QVals( args[0]);
			mpg = CcmSto.search4Label( QValMapSto.qval4AtomicLiteral( pooled, label), 0);
			if (null != mpg) {
				result[0] = mpg.oid;
				mpg = null;
			}
		}
			break;
		case QNEXT:
			result[0] = QValMapSto.NaN;
			mpg = CcmSto.searchNextLabel4Oid( args[0][0]);
			break;
		case QQL: {
			result[0] = QValMapSto.NaN;
			long cats = UiDataSto//.qSwitches[UiDataSto.SWI_MAPPING_CATS] & ((1L << 32) - 1L);
				.UI_FILTER_CATS.i64( null);
			String label = QValMapSto.string4QVals( args[0]);
			mpg = CcmSto.search4Label( cats, QValMapSto.qval4AtomicLiteral( pooled, label));
		}
			break;
		case QS: {
			long cats = UiDataSto //.qSwitches[UiDataSto.SWI_MAPPING_CATS] & ((1L << 32) - 1L);
				.UI_FILTER_CATS.i64( null);
			cats = (0 != cats) ? cats : Mapping.Cats.DEFAULT.flag;
			mpg = Mapping.make( QValMapSto.string4QVals( args[0]), Mapping.Cats.cats4Flags( cats), null, 0,
				QValMapSto.string4QVals( args[1]));
			if (0 == CcmSto.instance.add( null, mpg)) {
				result[0] = QValMapSto.NaN;
			} else {
				result = args[0];
			}
			mpg = null;
		}
			break;
		case QSTORE: {
			mpg = Mapping.make( QValMapSto.string4QVal( args[0][0]), QValMapSto.string4QVals( args[1]), null, 0,
				QValMapSto.string4QVals( args[2]));
			if (0 == CcmSto.instance.add( null, mpg)) {
				result[0] = QValMapSto.NaN;
			} else {
				result = args[0];
			}
			mpg = null;
		}
			break;
		case QUP:
			result[0] = QValMapSto.NaN;
			String oid = QValMapSto.string4QVals( args[0]);
			mpg = CcmSto.search4Oid( QValMapSto.qval4AtomicLiteral( pooled, oid));
			if (null != mpg) {
				if (0 != CcmSto.instance.update( mpg.clone4Data( args[1]))) {
					result = args[1];
				}
			}
			break;
		case RANDOM:
			len = (int) (((long) QValMapSto.doubleD4oQVal( args[0][0])) / 10000);
			///// Check memory
			try {
				result = new QVal[len];
				for (int i0 = 0; i0 < len; ++i0) {
					result[i0] = QValMapSto.qval4DoubleD4( QCalc.getNextRandom() * 1.0e4);
				}
			} catch (Throwable e) {
				result = QValMapSto.ERROR_SEQ;
			}
			break;
		case RANGE:
			len = args[0].length;
			num = (1 >= len) ? 0.0 : QValMapSto.doubleD4oQVal( args[0][0]);
			num2 = (2 >= len) ? 1.0e4 : QValMapSto.doubleD4oQVal( args[0][2]);
			len = (int) ((QValMapSto.doubleD4oQVal( args[0][(1 < len) ? 1 : 0]) - num) / num2 + 0.99999);
			len = (0 >= len) ? 1 : len;
			///// Check memory
			try {
				result = new QVal[len];
				for (int i0 = 0; i0 < len; ++i0) {
					result[i0] = QValMapSto.qval4DoubleD4( num);
					num += num2;
					num = MiscFunc.roundForIntUsage( num);
				}
			} catch (Throwable e) {
				result = QValMapSto.ERROR_SEQ;
			}
			break;
		case RCQ:
			//case RCX: zLastX
			str = QValMapSto.string4QVal( CcmSto.instance.variable_get( "Q"));
			result = QValMapSto.qvalAtoms4String( pooled, str);
			break;
		case STORE:
			str2 = QValMapSto.string4Literals( args[0]);
			str = QValMapSto.string4Literals( args[1]);
			if (1 == str.length()) {
				str = "M" + str.charAt( 0);
			}
			CcmSto.instance.variable_set( str, QValMapSto.qval4AtomicLiteral( pooled, str2));
			result = args[0];
			break;
		case STQ:
			CcmSto.instance.variable_force( "Q", args[0]);
			result = args[0];
			break;
		case SUM:
			num2 = 0.0;
			for (QVal v0 : args[0]) {
				num = QValMapSto.doubleD4oQVal( v0);
				if (!Double.isNaN( num)) {
					num2 += num;
				}
			}
			result[0] = QValMapSto.qval4DoubleD4( num2);
			break;
		case TICK:
			result[0] = QValMapSto.qval4DoubleD4( MiscFunc.currentTimeMillisLinearized() * 10.0);
			break;
		case VIEW:
			if ((0 >= args[0].length) || QValMapSto.isNumeric( args[0][0])) {
				Dib2Root.ui.feederCurrent = Dib2Root.ui.feederMain.get().switchFeeder( 1);
			} else {
				str = QValMapSto.string4QVals( args[0]).toUpperCase( Locale.ROOT);
				for (int i0 = Dib2Lang.kLanguages.length - 1; i0 >= 0; --i0) {
					if (str.startsWith( Dib2Lang.kLanguages[i0])) {
						Dib2Root.ui.iLang = i0;
					}
				}
			}
			return null;
		case ZRXG:
		case ZRXN:
		case ZRXT:
			result[0] = QCalc.doRegEx( pooled, QValMapSto.string4QVals( args[0]), QValMapSto.string4QVals( args[1]),
				(int) (QValMapSto.doubleD4oQVal( args[2][0])) / 10000, op);
			break;
		case ZRX:
			result[0] = QCalc.doRegEx( pooled, QValMapSto.string4QVals( args[0]), QValMapSto.string4QVals( args[1]),
				0.0, op);
			break;
		case ZSPLIT:
			str2 = QValMapSto.string4Literals( args[1]);
			str = QValMapSto.string4Literals( args[0]);
			result = QValMapSto.qvals4Strings( pooled, str.split( str2, -1));
			break;
		default:
			// cmd = null;
			return null;
		}
		if (null != mpg) {
			result[0] = QValMapSto.qval4String( pooled, QValMapSto.formatList( mpg.atDataElements, "", "\t", "", false));
		}
	} catch (Exception e) {
		result = null;
	}
	return result;
}

private boolean fastExec4( QValToken xyCmd, QVal[][] args) {
	switch (xyCmd.operator) {
	case DUP:
		xyCmd.val = args[0];
		xyCmd.result1.val = args[0];
		return true;
	case SWAP:
		xyCmd.val = args[1];
		xyCmd.result1.val = args[0];
		return true;
	default:
		;
	}
	return false;
}

private QVal[] MOVE_ME_TO_SLOW__Exec( QCalc funct, QVal[][] args) {
	QVal[] result = new QVal[1];
	if ((null == args) || (0 >= args.length)) {
		return null;
	}
	try {
		String file;
		byte[] dat;
		boolean ok;
		switch (funct) {
		case FDECR:
		case FENCR:
			ok = false;
			//				if ((null != args) && !QMap.isEmpty( QVal.asQVal( args[ 0 ] ) )) {
			file = QValMapSto.string4QVals( args[0]);
			if ((null != file) && (0 < file.length())) {
				try {
					if (0 > file.indexOf( '/')) {
						file = new File( Dib2Root.platform.getFilesDir( "external"), file).getAbsolutePath();
					}
					if (QCalc.FDECR == funct) {
						dat = TcvCodec.instance.readPacked( file + ".dib", null, null, null);
						MiscFunc.writeFile( file, dat, 0, dat.length, null);
					} else {
						dat = MiscFunc.readFile( file, 0);
						ok = (0 < TcvCodec.instance.writePacked( dat, 0, dat.length, file + ".dib"));
					}
				} catch (Exception e) {
				}
			}
			result[0] = ok ? QValMapSto.TRUE : QValMapSto.FALSE;
			break;
		case EXPALL:
		case EXPORT:
			ok = false;
			file = QValMapSto.string4QVals( args[0]);
			if ((null != file) && (0 < file.length())) {
				// Do not expose key values etc. unless really requested
				dat = CcmSto.instance.toCsv( null, 0, ~0, (funct == QCalc.EXPALL) ? (~0) : 4);
				try {
					if (0 > file.indexOf( '/')) {
						file = new File( Dib2Root.platform.getFilesDir( "external"), file).getAbsolutePath();
					}
					MiscFunc.writeFile( file, dat, 0, dat.length, null);
					ok = true;
				} catch (Exception e) {
				}
			}
			result[0] = ok ? QValMapSto.TRUE : QValMapSto.FALSE;
			break;
		case IMPORT:
			ok = false;
			file = QValMapSto.string4QVals( args[0]);
			if ((null != file) && (0 < file.length())) {
				try {
					if (0 > file.indexOf( '/')) {
						file = new File( Dib2Root.platform.getFilesDir( "external"), file).getAbsolutePath();
					}
					dat = MiscFunc.readFile( file, 0);
					if (Dib2Constants.MAGIC_BYTES.length < dat.length) {
						if ((byte) Dib2Constants.RFC4880_EXP2 == dat[0]) {
							ok = 0 < CcmSto.instance.importFile( file, false);
						} else if (Arrays.equals( Dib2Constants.MAGIC_BYTES,
							Arrays.copyOf( dat, Dib2Constants.MAGIC_BYTES.length))) {
							ok = 0 < CcmSto.instance.importCsv( dat, false, 0);
						} else {
							///// Expecting simple entries or a header line.
							boolean found = false;
							for (int i0 = 0; (i0 < 100) && (i0 < dat.length); ++i0) {
								if ('\t' == dat[i0]) {
									found = true;
									break;
								}
							}
							for (int i0 = 0; (i0 < 1000) && (i0 < dat.length); ++i0) {
								if ('\n' == dat[i0]) {
									found = found && true;
									break;
								}
							}
							if (found) {
								ok = 0 < CcmSto.instance.importCsv( dat, false, 0);
							}
						}
					}
				} catch (Exception e) {
				}
			}
			result[0] = ok ? QValMapSto.TRUE : QValMapSto.FALSE;
			break;
		case SAVTO:
			ok = false;
			file = QValMapSto.string4QVals( args[0]);
			if ((null != file) && (0 < file.length())) {
				if (0 > file.indexOf( '/')) {
					file = new File( Dib2Root.platform.getFilesDir( "external"), file).getAbsolutePath();
				}
				ok = 0 < CcmSto.instance.write( file, true, false, true);
			}
			result[0] = ok ? QValMapSto.TRUE : QValMapSto.FALSE;
			break;
		default:
			return null;
		}
	} catch (Exception e) {
		result = null;
	}
	//	cmd = null;
	return ((null == result) || ((1 <= result.length) && (null == result[0]))) ? null : result;
}

//TODO Make use of PENDING/ ExecSlow
@Override
public //
QValToken exec( QValToken... tasks) {
	if (1 != tasks.length) {
		return null;
	}
	QValToken cmd = tasks[0];

	///// Check if task applies and subtasks are finished.
	if ((null == cmd) || (null == cmd.operator) || (null == cmd.args4Lazy)) {
		return null;
	}
	if (cmd.operator.cArgs > cmd.args4Lazy.length) {
		return QValToken.EMPTY;
	}
	if (cmd.operator.zipped && (0 < cmd.args4Lazy.length) && (0 == cmd.cArgsZippedTodo)) {

		///// Check if subtasks are finished.
		int max = 0;
		for (int i0 = 0; i0 < cmd.operator.cArgs; ++i0) {
			if (null == cmd.args4Lazy[i0]) {
				return QValToken.EMPTY;
			} else if (null == cmd.args4Lazy[i0].val) {
				if (null == cmd.args4Lazy[i0].args4Lazy) {
					return QValToken.EMPTY;
				}
				if (cmd == WkRunner.INSTANCE.qPending) {
					WkRunner.INSTANCE.qPending = cmd.args4Lazy[i0];
				}
				return null;
			} else if (0 >= cmd.args4Lazy[i0].val.length) {
				return QValToken.EMPTY;
			}
			max = (max >= cmd.args4Lazy[i0].val.length) ? max : cmd.args4Lazy[i0].val.length;
		}
		// Check memory ...
		try {
			cmd.val = new QVal[max];
		} catch (Throwable e) {
			cmd.errorTrace = new String[] { "Memory" };
			cmd.val = QValMapSto.ERROR_SEQ;
			return cmd;
		}
		cmd.cArgsZippedTodo = max;
	}

//	QVal[] result = null;
	if (0 >= cmd.cArgsZippedTodo) {
		QVal[][] args = new QVal[cmd.operator.cArgs][];
		for (int i0 = 0; i0 < cmd.operator.cArgs; ++i0) {
			args[i0] = cmd.args4Lazy[i0].val;
		}
		if (1 < cmd.operator.cReturnValues) {
			fastExec4( cmd, args);
		} else {
//	final JResult pooled = JResult.get8Pool();
			cmd.val = fastExec( cmd.operator, args);
			if (null == cmd.val) {
				cmd.val = cmd.operator.calc( (0 < args.length) ? args[0] : null);
			}
			if (null == cmd.val) {
				cmd.val = MOVE_ME_TO_SLOW__Exec( cmd.operator, args);
			}
		}
	} else {
		QVal[] args = new QVal[cmd.operator.cArgs];
		for (int maxSteps = 10; (0 < cmd.cArgsZippedTodo) && (0 < maxSteps); --maxSteps, --cmd.cArgsZippedTodo) {
			for (int i0 = 0; i0 < cmd.operator.cArgs; ++i0) {
				final int len = cmd.args4Lazy[i0].val.length;
				args[i0] = cmd.args4Lazy[i0].val[(len - cmd.cArgsZippedTodo) % len];
			}
			QVal[] result = cmd.operator.calc( args);
			if (0 < result.length) {
				cmd.val[cmd.val.length - cmd.cArgsZippedTodo] = result[0];
				if ((1 < result.length) && (null != cmd.result1)) {
					cmd.result1.val[cmd.val.length - cmd.cArgsZippedTodo] = result[1];
				}
			}
		}
		if (0 < cmd.cArgsZippedTodo) {
			return cmd;
		}
	}
	cmd.cArgsZippedTodo = 0;
	cmd.operator = null;
	cmd.args4Lazy = null;
	cmd.result1 = null;
	cmd.result2 = null;
	return cmd;
}

//=====
}
