// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_x;

import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.QCalc;

/** Long-running process. */
public final class ExecSlowPrcs implements QExec.QDspPrcsIf {
//=====

///// Threaded (in/ out/ trigger)

public final QPlace wxCommands = new QPlace();
private QPlace rResults;

/////

private QValToken mIn;
private QValToken mOut = null;
private double[] mSlowIntermediate;
private long[] mSlowParameters;

@Override
public boolean init( QPlace... xrResult) {
	rResults = xrResult[0];
	return true;
}

@Override
public QValToken peek( long... xbOptFlags) {
	return wxCommands.peek();
}

//TODO
private boolean start() {
	if (null == mIn) {
		return false;
	}
	mOut = null;
	QValToken out = null;
	try {
//		boolean delay = true; //(0 >= msecLimit);
//		///// For the fun of it ...
//		double factArg = ((QCalc.FACT == mIn.operator) && (null != mIn.vals) //,
//			&& QMapSto.isNumeric( mIn.vals[0][0])) ? ...QMapSto.doubleD4oQVal( mIn.vals[0][0]) : 0.0;
//		delay = delay || ((100.0 < factArg) && (factArg < 170.0)); // && (200 > msecLimit));
//		if (delay) {
//			mSlowParameters = new long[2];
//			mSlowIntermediate = new double[1];
//			mSlowParameters[0] = 1;
//			mSlowParameters[1] = (long) factArg;
//			mSlowParameters[1] = (170 < mSlowParameters[1]) ? 170 : mSlowParameters[1];
//			mSlowIntermediate[0] = 1.0;
//			return true;
//		} else {

//		final QVal[] result = mIn.operator.calc( mIn.vArgs[0]);
//		out = new QTkTask();
//		out.vArgs = new QVal[][] { result };
//		}
	} catch (Exception e) {
		return false;
	}

	mOut = out;
	return true;
}

@Override
public int step() {
	QValToken out = mOut;
	mOut = null;
	if (null == out) {
		if (null == mIn) {
			mIn = (QValToken) wxCommands.pull();
			start();
			if (null == mIn) {
				return 0; // mOut;
			}
		}
		if (QCalc.FACT == mIn.operator) {
			// for testing
			try {
				Thread.sleep( 200);
			} catch (InterruptedException e) {
			}
			for (long to = mSlowParameters[0] + 10; (mSlowParameters[0] < to) && (mSlowParameters[0] <= mSlowParameters[1]);
				++mSlowParameters[0]) {
				mSlowIntermediate[0] *= mSlowParameters[0];
			}
			if (mSlowParameters[0] < mSlowParameters[1]) {
				return 0; //QTkTask.PENDING;
			}
			out = new QValToken();
//			out.vArgs = new QVal[][] { new QVal[] { QValMapSto.qval4DoubleD4( mSlowIntermediate[0]) } };
		}
	}
	// TODO push
	return 0; //out;
}

@Override
public boolean requested() {
	// TODO Auto-generated method stub
	return false;
}

@Override
public int prepare( long... parameters) {
	// TODO Auto-generated method stub
	return 0;
}

//=====
}
