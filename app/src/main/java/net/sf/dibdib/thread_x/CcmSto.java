// Copyright (C) 2016,2017,2018,2019,2020  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_x;

import java.io.*;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_any.QValFunc.QVal;

/** Concept mapping, saved as
 * CSV with tabs (TSV, commas if tabs missing) as single line (trailing '\n' as '\t').
 */
public final class CcmSto {
//=====

public static CcmSto instance = new CcmSto();

private long zLastSave = 0;
private final boolean zLoadSuccess = false;
//private final boolean zStray = false;
/** To be saved as hex literals! (with or without leading "X'") */
private ConcurrentHashMap<String, byte[]> zPrefs = null;
/** X/Y/Z ==> stack, T=Term, M=Memory, L=Last, G=Graphics */
private ConcurrentHashMap<String, String> zVariables = null; // TODO use QMap
private long[] zahContactsNGroups = new long[100];
private int zcContactsNGroups = 0;
private QValToken[] zStack = null; //QVal[][] zStack = null;
private int zcStack = 0;

public CcmSto() {
	zPrefs = new ConcurrentHashMap<String, byte[]>();
	zVariables = new ConcurrentHashMap<String, String>();
	zStack = new QValToken[100]; //new QVal[100][];
	zPrefs.put( "pub", "".getBytes( StringFunc.STR256));
	zPrefs.put( "sec", "".getBytes( StringFunc.STR256));
}

public boolean isInitialized() {
	return 1000 < zLastSave;
}

public byte[] preference_get( String key) {
	key = key.replace( '\t', ' ').replace( '\n', ' ');
	if (".".equals( key)) {
		// Even if qRootAddress will later be used as value:
		return StringFunc.bytesUtf8( QValMapSto.string4QVal( Mapping.qhRootOid));
	} else if ("email_address".equals( key)) {
		return StringFunc.bytesUtf8( Mapping.qRootAddress);
	} else if ("lastId".equals( key)) {
		return MiscFunc.createId( key).getBytes( StringFunc.STR256);
	}
	byte[] out = zPrefs.get( key);
	return (out == null) ? null : out.clone();
}

public String preference_getHex( String key, boolean marked) {
	key = key.replace( '\t', ' ').replace( '\n', ' ');
	if (".".equals( key) || "email_address".equals( key) || "lastId".equals( key)) {
		return StringFunc.hex4Bytes( preference_get( key), marked);
	}
	byte[] out = zPrefs.get( key);
	return (null == out) ? "" : StringFunc.hex4Bytes( out, marked);
}

public synchronized void preference_set( String key, byte[] value, QVal oid4ForcingInitial) {
	key = key.replace( '\t', ' ').replace( '\n', ' ').trim();
	if (null == value) {
		zPrefs.remove( key);
	} else if (".".equals( key) || "email_address".equals( key)) {
		if (((null == Mapping.qRootAddress) || (null != oid4ForcingInitial)) && (6 <= value.length)) {
			final JResult pooled = JResult.get8Pool();
			Mapping.qRootAddress = StringFunc.string4Utf8( value);
			// Expect Mapping objects to have 0 as contributor OID, so that this change
			// does not 'hurt':
			Mapping.qhRootOid = (null != oid4ForcingInitial) ? oid4ForcingInitial
				: QValMapSto.qval4AtomicLiteral( pooled, MiscFunc.createId( StringFunc.string4Utf8( value)));
		}
		if (null != oid4ForcingInitial) {
			if (".".equals( key)) {
				Mapping.qhRootOid = oid4ForcingInitial;
				if (!StringFunc.string4Utf8( value).equalsIgnoreCase( Mapping.qRootAddress)) {
					//TODO
				}
			} else if ("email_address".equals( key)) {
				// For new root OID or keep OID for new address!
				String email = StringFunc.string4Utf8( value);
				Mapping.qRootAddress = email;
				///// Has to match main CONTACT
				Object dat0 = Mapping.qhRootOid; //QMap.main.readStructElement( Mapping.qhRootOid, 0 );
				if ((null == dat0) || !(dat0 instanceof String) || !((String) dat0).contains( email)) { //&& (mpg instanceof Mapping)) {
					//TODO search CONTACTs for e-mail address, or add or replace
				}
			}
		}
	} else if ("lastId".equals( key)) {
		if (8 < value.length) {
			MiscFunc.initLastId( new String( value, StringFunc.STR256));
		}
	} else if (0 < key.length()) {
		zPrefs.put( key, value.clone()); //MiscFunc.toHexLiteral( value, true ) );
	}
	//	write();
}

public synchronized void preference_setHex( String key, String hexLiteral, QVal oid4ForcingInitial) {
	byte[] val = null;
	if (null == hexLiteral) {
	} else if (0 >= hexLiteral.length()) {
		val = new byte[0];
	} else {
		char ch = hexLiteral.charAt( 0);
		if (('X' == ch) && hexLiteral.startsWith( "X'")) {
			val = StringFunc.bytes4Hex( hexLiteral);
		} else if (((('0' <= ch) && (ch <= '9')) || (('A' <= ch) && (ch <= 'F'))) && hexLiteral.matches( "[0-9A-F ]+")) {
			// Bad luck if it is meant to be a decimal number.
			val = StringFunc.bytes4Hex( hexLiteral);
		} else {
			// Not hex after all.
			val = StringFunc.bytesUtf8( hexLiteral);
		}
	}
	preference_set( key, val, oid4ForcingInitial);
}

public synchronized void preference_remove( String key) {
	Dib2Root.log( "preference_remove", key);
	zPrefs.remove( key);
}

public QVal variable_get( String name) {
	final JResult pooled = JResult.get8Pool();
	return QValMapSto.qval4String( pooled, zVariables.get( name));
}

/**
 * Set variable. Name is expected to have more than 1 char for user variables.
 * @param name Name of variable.
 * @param value null iff variable is to be removed.
 */
public synchronized void variable_set( String name, QVal value) {
	name = StringFunc.nameNormalize( name, 0xff);
	///// Reserved?
	if (1 >= name.length()) {
		//e.g. '.' for ROOT
		return;
	} else if (('X' <= name.charAt( 0)) && ('Z' >= name.charAt( 0)) && (name.substring( 1).matches( "[0-9]+"))) {
		return;
	}
	if (null == value) {
		zVariables.remove( name);
	} else {
		zVariables.put( name, QValMapSto.string4QVal( value));
	}
}

public synchronized void variable_remove( String name) {
	name = StringFunc.nameNormalize( name, 0xff);
	zVariables.remove( name);
}

public void variable_force( String name, QVal[] value) {
	final JResult pooled = JResult.get8Pool();
	name = StringFunc.nameNormalize( name, 0xff);
	int iTo = -1;
	if (1 >= name.length()) {
		if (0 >= name.length()) {
			return;
		} else if (('X' <= name.charAt( 0)) && ('Z' >= name.charAt( 0))) {
			iTo = name.charAt( 0) - 'X';
		} else if (name.equals( ".")) {
			return;
		}
	} else if (name.substring( 1).matches( "[0-9]+")) {
		if (('X' <= name.charAt( 0)) && ('Y' >= name.charAt( 0))) {
			return;
		} else if (('Z' == name.charAt( 0))) {
			iTo = Integer.parseInt( name.substring( 1)) + 2;
			if (iTo >= (zStack.length - 1)) {
				return;
			}
		}
	}
	if (0 <= iTo) {
		int e0 = iTo + 1 - zcStack;
		if (0 < e0) {
			System.arraycopy( zStack, 0, zStack, e0, zcStack);
			for (int ix = e0 - 1; ix >= 0; --ix) {
				zStack[ix] = QValToken.create( QValMapSto.NIL_SEQ);
			}
			zcStack = iTo + 1;
		}
		QValMapSto.storeQVals( pooled, value);
		zStack[zcStack - 1 - iTo] = QValToken.create( value);
		return;
	}
	zVariables.put( name, QValMapSto.string4QVals( value));
}

public int stackSize() {
	return zcStack;
}

public synchronized int stackPush( QValToken task) {
	zLastSave = (1000 >= zLastSave) ? 1001 : zLastSave;
	if (null == task) {
		Dib2Root.log( "CsvDb:stackPush null");
		return -1;
	}
	if (zcStack >= zStack.length) {
		zStack = Arrays.copyOf( zStack, 2 * zStack.length);
	}
	zStack[zcStack++] = task;
	return zcStack - 1;
}

public synchronized int stackPush( QVal... value) {
	zLastSave = (1000 >= zLastSave) ? 1001 : zLastSave;
	if ((null == value) || (0 == value.length)) {
		if (null == value) {
			Dib2Root.log( "CsvDb:stackPush null");
		}
		return -1;
	}
	final JResult pooled = JResult.get8Pool();
	QValMapSto.storeQVals( pooled, value);
	if (zcStack >= zStack.length) {
		zStack = Arrays.copyOf( zStack, 2 * zStack.length);
	}
	zStack[zcStack++] = QValToken.create( value);
	return zcStack - 1;
}

public int stackReplace( int pos, QVal... value) {
	if ((0 <= pos) && (pos < zcStack)) {
		zStack[pos] = QValToken.create( value);
		return pos;
	}
	return -1;
}

public synchronized int stackRemove( int pos) {
	if ((0 <= pos) && (pos < zcStack)) {
		--zcStack;
		System.arraycopy( zStack, pos + 1, zStack, pos, zcStack - pos);
		return pos;
	}
	return -1;
}

public synchronized int stackInsert( int pos, QVal[][] values) {
	final int len = values.length;
	final JResult pooled = JResult.get8Pool();
	for (QVal[] value : values) {
		if (null == value) {
			Dib2Root.log( "CsvDb:stackInsert null");
		} else {
			QValMapSto.storeQVals( pooled, value);
		}
	}
	if ((pos > zcStack) || (0 > pos)) {
		pos = zcStack;
	}
	zcStack += len;
	while (zcStack >= zStack.length) {
		zStack = Arrays.copyOf( zStack, 2 * zStack.length);
	}
	if ((pos + 1) < (zcStack - len)) {
		System.arraycopy( zStack, pos, zStack, pos + len, zcStack - len);
	}
	System.arraycopy( values, 0, zStack, pos, len);
	return pos;
}

public synchronized void stackClear( boolean clearMemVariables) {
	zcStack = 0;
	if (clearMemVariables) {
		for (char iMemory = '0'; iMemory <= 'z'; ++iMemory) {
			variable_remove( "M" + iMemory);
		}
	}
}

public QValToken stackPeek( int pos) {
	final QValToken[] stack = zStack;
	final int cnt = zcStack;
	return ((0 >= cnt) || (pos >= cnt) || (null == stack[cnt - 1 - pos])) ? null : stack[cnt - 1 - pos];
}

public synchronized QValToken[] stackPop( int cArgs, QCalc cmd) {
	if (zcStack < cArgs) {
		return null;
	}
	final JResult pooled = JResult.get8Pool();
	if (null != cmd) {
		final QValToken[] stack = zStack;
		QVal operator = QValMapSto.qval4String( pooled, cmd.getOperatorOrName());
		QVal[] term = new QVal[cArgs * 2 + 1];
		if ((0 < cArgs) && (0 < zcStack) && (null != stack[zcStack - 1]) && (null != stack[zcStack - 1].val)) {
			variable_force( "L", zStack[zcStack - 1].val);
		}
		for (int i0 = zcStack - cArgs, i1 = 0; i0 < zcStack; ++i0, ++i1) {
			if ((null == stack[i0]) || (null == stack[i0].val)) {
				term[i1] = QValMapSto.qval4AtomicLiteral( pooled, "'^'");
			} else if (1 == stack[i0].val.length) {
				term[i1] = stack[i0].val[0];
			} else {
				term[i1] = QValMapSto.qval4String( pooled, QValMapSto.string4QVal( stack[i0].val[0]) + "...");
			}
			QValMapSto.storeQVals( pooled, term[i1]);
			term[++i1] = QValMapSto.V_BLANK;
		}
		term[term.length - 1] = operator;
		variable_force( "T", term);
	}
	QValToken[] out = Arrays.copyOfRange( zStack, zcStack - cArgs, zcStack);
	zcStack -= cArgs;
	if ((100 <= zcStack) && (zStack.length / 2 > zcStack)) {
		zStack = Arrays.copyOf( zStack, zcStack + 10);
	}
	return out;
}

public synchronized int stackCopy( QVal[][] yData, int offset, boolean partial) {
	if (!partial && (zcStack > (yData.length - offset))) {
		return -zcStack - offset;
	}
	int to = offset;
	for (int next = zcStack - 1; (next >= 0) && (to < yData.length); --next, ++to) {
		yData[to] = zStack[next].val;
	}
	if (to < yData.length) {
		yData[to] = null;
	}
	return to;
}

public synchronized int stackRead( long xbHexMemory, String[] yLines, int offset, boolean partial) {
	if (!partial && (zcStack > (yLines.length - offset))) {
		return -zcStack - offset;
	}
	int to = offset;
	int ceil = zcStack + offset; //stackCopy( yLines, offset + 1, partial );
	final QValToken[] stack = zStack;
	for (int next = zcStack - 1; (next >= 0) && (to < yLines.length); --next, ++to) {
		if (((yLines.length - 1) == to) && ((ceil - 1) > to)) {
			yLines[to] = "...";
			continue;
		}
		int i0 = to - offset - 2;
		String key = (-2 == i0) ? "X" : ((-1 == i0 ? "Y" : ((0 == i0 ? "Z" : ("Z" + i0)))));
		if ((null == stack[next]) || (null == stack[next].val)) {
			yLines[to] = "^";
		} else {
			yLines[to] = //zStack[ next ].format( key, "\t", (0 != (1 & xbHexMemory)) );
				key + QValMapSto.formatList( stack[next].val, "\t", " ", "", (0 != (1 & xbHexMemory)));
		}
	}
	if (0 != (2 & xbHexMemory)) {
		if ((10 > zVariables.size()) && ((yLines.length - ceil) > zVariables.size())) {
			for (String var : zVariables.keySet()) {
				yLines[to++] = var + '\t' + zVariables.get( var);
			}
		} else {
			char iMemory = '0';
			for (; (iMemory <= 'z') && (to < yLines.length); ++iMemory) {
				if (zVariables.containsKey( "M" + iMemory)) {
					yLines[to++] = "M" + iMemory + '\t' + zVariables.get( "M" + iMemory);
				}
			}
		}
	}
	if (to < yLines.length) {
		yLines[to] = null;
	}
	return to;
}

public synchronized/*h*/long add( Mapping.Cats optionalCat, Mapping mpg) {
	final JResult pooled = JResult.get8Pool();
	if (Mapping.Cats.PREF == optionalCat) {
		String val = (0 >= mpg.atDataElements.length) ? "" : QValMapSto.string4QVal( mpg.atDataElements[0]);
		zPrefs.put( QValMapSto.string4QVal( mpg.uLabel), StringFunc.bytes4Hex( val));
		return QValMapSto.STRING_ERROR;
	}
	if ((null == optionalCat) && (0 != ((Mapping.Cats.CONTACT.flag | Mapping.Cats.GROUP.flag) & mpg.bCategories))) {
		optionalCat = Mapping.Cats.CONTACT;
	}
	if ((null != optionalCat) && (0 == (optionalCat.flag & mpg.bCategories))) {
		optionalCat = null;
	}
	final long h0 =
		QValMapSto.cacheAggreg4QVals( pooled, QValMapSto.LIST, Dib2Constants.CSVDB_MAP_INDEX, mpg.toQVals( false));
	long handle = QValMapSto.storeCachedAggreg( pooled, Dib2Constants.CSVDB_MAP_INDEX, h0,
		QValMapSto.string4QVal( mpg.oid));
	if ((Mapping.Cats.CONTACT == optionalCat) || (Mapping.Cats.GROUP == optionalCat)) {
		if (zcContactsNGroups >= zahContactsNGroups.length) {
			zahContactsNGroups = Arrays.copyOf( zahContactsNGroups, 2 * zahContactsNGroups.length);
		}
		zahContactsNGroups[zcContactsNGroups++] = handle;
	}
	return handle;
}

public synchronized boolean remove( QVal xOid, boolean force) {
	final JResult pooled = JResult.get8Pool();
	long[] handles = QValMapSto.findStored( pooled, Dib2Constants.CSVDB_MAP_INDEX, 1, QValMapSto.string4QVal( xOid));
	if ((null == handles) || (1 != handles.length)) {
		if (!force) {
			return false;
		}
	}
	for (long handle : handles) {
		QValMapSto.removeStored( Dib2Constants.CSVDB_MAP_INDEX, handle);
	}
	return true;
}

public synchronized/*h*/long update( Mapping xMpg) {
	final JResult pooled = JResult.get8Pool();
	if (0 != (Mapping.Cats.PREF.flag & xMpg.bCategories)) {
		String val = (0 >= xMpg.atDataElements.length) ? "" : QValMapSto.string4QVal( xMpg.atDataElements[0]); //.getItem( 0 ).toStringFull( "" );
		zPrefs.put( QValMapSto.string4QVal( xMpg.uLabel), StringFunc.bytes4Hex( val));
		return QValMapSto.STRING_ERROR;
	}
	long[] hOld = QValMapSto.findStored( pooled, Dib2Constants.CSVDB_MAP_INDEX, 1, QValMapSto.string4QVal( xMpg.oid));
	if ((null == hOld) || (0 >= hOld.length)) {
		return QValMapSto.STRING_ERROR;
	}
	//TODO keep old version
	remove( xMpg.oid, true);
	int iOld = zcContactsNGroups - 1;
	for (; iOld >= 0; --iOld) {
		if (zahContactsNGroups[iOld] == hOld[0]) {
			break;
		}
	}
	if (0 == ((Mapping.Cats.CONTACT.flag | Mapping.Cats.GROUP.flag) & xMpg.bCategories)) {
		if (0 <= iOld) {
			zahContactsNGroups[iOld] = zahContactsNGroups[zcContactsNGroups - 1];
			--zcContactsNGroups;
		}
	} else if (0 > iOld) {
		if (zcContactsNGroups >= zahContactsNGroups.length) {
			zahContactsNGroups = Arrays.copyOf( zahContactsNGroups, 2 * zahContactsNGroups.length);
		}
		zahContactsNGroups[zcContactsNGroups++] = hOld[0];
	}
	return add( null, xMpg);
}

public static Mapping search4Oid( QVal oid) {
	final JResult pooled = JResult.get8Pool();
	long[] hOld = QValMapSto.findStored( pooled, Dib2Constants.CSVDB_MAP_INDEX, 1, QValMapSto.string4QVal( oid));
	if ((null == hOld) || (0 >= hOld.length)) {
		return null;
	}
	return new Mapping( oid, QValMapSto.qvals4Aggreg(
		//QMapSto.readHandles4StoredAggreg( 
		Dib2Constants.CSVDB_MAP_INDEX, hOld[0])); //.getItems() );
}

/** Search entry ...
 * @param preferredCats
 * @param label As literal!
 * @return
 */
public static Mapping search4Label( long preferredCats, QVal label) {
	final JResult pooled = JResult.get8Pool();
	final String shash4Label = QValMapSto.shash4QVal( label) + (char) 1; //QStr.shash( label ) + (char) 1;
	final long[] handles = QValMapSto.traverse( pooled, Dib2Constants.CSVDB_MAP_INDEX, shash4Label, 0);
	Mapping out = null;
	if (null == handles) {
		return null;
	}
	for (long handle : handles) {
		QVal[] list = QValMapSto.qvals4Aggreg( Dib2Constants.CSVDB_MAP_INDEX, handle);
		if (null == list) {
			continue;
		} else if (QValMapSto.equals( -1, QVal.asQVal( label),
			QVal.asQVal( list[Mapping.Fields.LABEL.ordinal()]))) {
			String oid = QValMapSto.readKey( Dib2Constants.CSVDB_MAP_INDEX, handle, 1);
			if (null == oid) {
				continue;
			}
			out = new Mapping( QValMapSto.qval4AtomicLiteral( pooled, oid), list);
			if (preferredCats == (out.bCategories & preferredCats)) {
				break;
			}
		}
	}
	return out;
}

/** Search entry ...
 * @param label As literal!
 * @param index
 * @return
 */
public static Mapping search4Label( QVal label, int index) {
	final JResult pooled = JResult.get8Pool();
	final String shash4Label = QValMapSto.shash4QVal( label) + (char) 1; //QStr.shash( label ) + (char) 1;
	final long[] handles = QValMapSto.traverse( pooled, Dib2Constants.CSVDB_MAP_INDEX, shash4Label, 0);
	if (null == handles) {
		return null;
	}
	int i0 = 0;
	for (long handle : handles) {
		QVal[] list = QValMapSto.qvals4Aggreg( Dib2Constants.CSVDB_MAP_INDEX, handle);
		if (null == list) {
			continue;
		} else if (QValMapSto.equals( -1, QVal.asQVal( label),
			QVal.asQVal( list[Mapping.Fields.LABEL.ordinal()]))) {
			++i0;
			if (i0 <= index) {
				continue;
			}
			String oid = QValMapSto.readKey( Dib2Constants.CSVDB_MAP_INDEX, handle, 1);
			if (null == oid) {
				continue;
			}
			return new Mapping( QValMapSto.qval4AtomicLiteral( pooled, oid), list);
		}
	}
	return null;
}

public static Mapping searchNextLabel4Oid( QVal xOid) {
	final JResult pooled = JResult.get8Pool();
	Mapping mpg = search4Oid( xOid);
	if (null == mpg) {
		return null;
	}
	final QVal label = mpg.uLabel;
	final String shash4Label = QValMapSto.shash4QVal( label) + (char) 1;
	final long[] handles = QValMapSto.traverse( pooled, Dib2Constants.CSVDB_MAP_INDEX, shash4Label, 0);
	if (null == handles) {
		return null;
	}
	boolean found = false;
	final long hOid = QVal.asQVal( xOid);
	for (long handle : handles) {
		String oid = QValMapSto.readKey( Dib2Constants.CSVDB_MAP_INDEX, handle, 1);
		if (null == oid) {
			continue;
		}
		if (!found) {
			if (QValMapSto.equalValues( hOid, oid)) {
				found = true;
			}
		} else {
			QVal[] list = QValMapSto.qvals4Aggreg( Dib2Constants.CSVDB_MAP_INDEX, handle);
			if (null == list) {
				continue;
			}
			if (QValMapSto.equals( -1, QVal.asQVal( label),
				QVal.asQVal( list[Mapping.Fields.LABEL.ordinal()]))) {
				return new Mapping( QValMapSto.qval4AtomicLiteral( pooled, oid), list);
			}

		}
	}
	return null;
}

//public-- synchronized--: thread local. 
void save( String path, boolean immediately) {
	write( path, immediately, true, false);
}

public synchronized //TODO 
int write( String path, boolean immediately, boolean backupOld, boolean includeTrash) {
	byte[] phrase = TcvCodec.instance.getPassFull();
	if (null == phrase) {
		return -1;
	}
	if (!immediately && ((zLastSave + 60 * 1000) >= MiscFunc.currentTimeMillisLinearized())) {
		return -1;
	}
	Dib2Root.log( "save", " .. " + zPrefs.size());
	File pathFTemp = new File( path + ".tmp");
	if (pathFTemp.isFile()) {
		pathFTemp.delete();
	}
	zLastSave = MiscFunc.currentTimeMillisLinearized();
	byte[] dat = toCsv( null, 0, ~0, includeTrash ? (~0) : (~2));
	Dib2Root.log( "exportLines", "ok " + dat.length);

	int len = TcvCodec.instance.writePacked( dat, 0, dat.length, pathFTemp.getAbsolutePath()); //, optVersion);
	Dib2Root.log( "save", "ok? " + len);
	if (0 <= len) {
		File pathFNew = new File( path);
		if (pathFNew.isFile()) {
			if (backupOld) {
				path = pathFNew.getAbsolutePath();
				File old = new File( path + (immediately ? ".old" : ".bak"));
				if (old.exists()) {
					if (!immediately || !zLoadSuccess) {
						old = new File( path + ".bak");
					}
					old.delete();
				}
				pathFNew.renameTo( old);
			} else {
				pathFNew.delete();
			}
		}
		pathFTemp.renameTo( pathFNew);
	}
	zLastSave = MiscFunc.currentTimeMillisLinearized();
	return len;
}

//public-- synchronized--: thread local. 
int load( String path) {
	int out = -1;
	byte[] phrase = TcvCodec.instance.getPassFull();
	if (null == path) {
		zLastSave = MiscFunc.currentTimeMillisLinearized();
		return (null == phrase) ? -1 : 0;
	}
	if (null == phrase) {
		return -1;
	}
	Dib2Root.log( "load", " .. " + zPrefs.size());
	out = importFile( path, true);
	return out;
}

/**
 * Import encoded file.
 * @param filePath Path with name of file
 * @param phrase Pass phrase
 * @param replace true for overriding everything
 * @return number of imported records or -1
 */
public synchronized //TODO
int importFile( String filePath, boolean replace) {
	byte[] header = new byte[8];
	byte[] dat = TcvCodec.instance.readPacked( filePath, header, null, null);
	if (null == dat) {
		Dib2Root.log( "import", "read/decode failed.");
		return -1;
	}
	int version = header[2] & 0xff;
	version = (6 >= version) ? (10 * version) : header[4];
	int count = 0;
	int flagsMarkAdjusttimeKeyhex = (replace ? 0 : 2) | ((30 >= version) ? 1 : 0);
	flagsMarkAdjusttimeKeyhex |= (50 > version) ? 4 : 0;
	try {
		count = importCsv( dat, replace, flagsMarkAdjusttimeKeyhex);
	} catch (Exception e) {
		Dib2Root.log( "import", e.getMessage());
		return -1;
	}
	Dib2Root.log( "import", "" + count);
	return count;
}

private Object importCsv( byte[] csvData, boolean directly, boolean replace, int flagsMarkAdjusttimeKeyHex) {
	final JResult pooled = JResult.get8Pool();
	if (csvData.length <= 2) {
		return directly ? 0 : new Mapping[0];
	}
	int i1 = 1;
	int iOut = 0;
	int count = 0;
	int i0 = 0;
	int iOid = 0;
	boolean oidFound = false;
	boolean hasTimeStamp = true;
	Mapping[] out = new Mapping[24];
	if ((csvData[0] == Dib2Constants.MAGIC_BYTES[0]) && (csvData[1] == Dib2Constants.MAGIC_BYTES[1])) {
		// Skip header:
		i0 = 1 + MiscFunc.indexOf( csvData, new byte[] { '\n' });
		iOid = (csvData[2] == '\t') ? -1 : 0;
	} else {
		iOid = -2;
		i0 = 1 + MiscFunc.indexOf( csvData, new byte[] { '\n' });
		hasTimeStamp = new String( Arrays.copyOf( csvData, i0), StringFunc.STR256).contains( "\tTIME\t");
		if ((' ' >= csvData[0]) || hasTimeStamp) {
			// Skip header
		} else {
			i0 = 0;
		}
	}
	for (; i0 < csvData.length; i0 = i1 + 1) {
		i1 = MiscFunc.indexOf( csvData, new byte[] { '\n' }, i0);
		if (i1 < 0) {
			i1 = csvData.length;
		}
		String line;
		try {
			line = new String( csvData, i0, i1 - i0, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			line = new String( csvData, i0, i1 - i0);
		}
		if ((line.indexOf( "\t") < 0) && (line.indexOf( ",") > 0)) {
			line = line.replaceAll( "\"? *, *\"?", "\t");
		}
		String[] a0 = line.split( "\t"); //, 6 + iOid );
		if ((5 + iOid) > a0.length) {
			continue;
		}
		try {
			if (iOut >= out.length) {
				out = Arrays.copyOf( out, 2 * iOut);
			}
			final Mapping mpg = (-2 < iOid) ? new Mapping( a0, iOid, flagsMarkAdjusttimeKeyHex)
				: Mapping.make( a0[0], a0[1], (hasTimeStamp ? a0[2] : ""), (hasTimeStamp ? 3 : 2), a0);
			out[iOut] = mpg;
			if (directly) {
				if (0 != ((Mapping.Cats.PREF.flag | Mapping.Cats.VAR.flag) & mpg.bCategories)) {
					///// Pairs that are stored separately (PREF, VAR, ...).
					final String key = QValMapSto.string4QVal( mpg.uLabel);
					if (0 >= key.length()) {
						continue;
					}
					final String value =
						(0 >= mpg.atDataElements.length) ? "" : QValMapSto.string4QVal( mpg.atDataElements[0]);
					///// Do not override current entries when importing older data.
					if ((0 != (Mapping.Cats.PREF.flag & mpg.bCategories)) && (1 <= key.length())) {
						///// 'value' as hexstring!
						if (replace || !zPrefs.containsKey( key) || (null == zPrefs.get( key))
							|| (0 >= zPrefs.get( key).length)) {
							if (0 != (flagsMarkAdjusttimeKeyHex & 4) && key.startsWith( "KEY")
								&& (value.length() > 3 * "CAAU".length()) && value.matches( "3.3.3.3.*")) {
								preference_setHex( key, StringFunc.string4HexUtf8( value), null);
							} else {
								preference_setHex( key, value, replace ? mpg.oid : null);
							}
						}
					} else {
						if (replace || !zVariables.containsKey( key) || (null == zVariables.get( key))
							|| (0 >= zVariables.get( key).toString().length())) {
							if (key.equals( ".")) {
								if (replace) {
									// Wrong placement ...
									Mapping.qRootAddress = (4 <= value.length()) ? value : Mapping.qRootAddress;
									if (4 <= value.length()) {
										Mapping.qRootAddress = (value.startsWith( "X") || ('7' >= value.charAt( 0)))
											? StringFunc.string4HexUtf8( value) : value;
									}
									Mapping.qhRootOid = mpg.oid;
								}
							} else {
								variable_force( key, QValMapSto.qvalAtoms4String( pooled, value));
							}
						}
					}
				} else {
					///// Note: Not duplicating values if they are used as PREF or VAR.
					if (0 != (Mapping.Cats.CONTACT.flag & mpg.bCategories)) {
						// Expected as first part:
						String email = (0 >= mpg.atDataElements.length) ? "" : QValMapSto.string4QVal( mpg.atDataElements[0]);
						email = email.substring( 1 + email.indexOf( ' ')).trim();
						if (3 <= email.length() && (0 < email.indexOf( '@'))) {
							if (email.equalsIgnoreCase( Mapping.qRootAddress)) {
								if (replace || !oidFound) {
									Mapping.qhRootOid = mpg.oid; //QMap.main.makeHandle( pooled, mpg.oid );
									oidFound = true;
								}
							} else if (null == Mapping.qRootAddress) {
								Mapping.qRootAddress = email;
							}
						}
					}
					add( null, mpg);
				}
			} else {
				++iOut;
			}
			++count;
		} catch (Exception e) {
			Dib2Root.log( "Csv ", "Import failed/ " + count + ": " + e);
			return directly ? 0 : null;
		}
	}
	if (directly && (0 <= count)) {
		zLastSave = MiscFunc.currentTimeMillisLinearized();
	}
	return directly ? (Integer) count : Arrays.copyOf( out, iOut);
}

public synchronized Mapping[] fromCsv( byte[] csvData, int flagsMarkShift4Time) {
	return (Mapping[]) importCsv( csvData, false, false, flagsMarkShift4Time);
}

public synchronized int importCsv( byte[] csvData, boolean replace, int flagsMarkShift4Time) {
	return (Integer) importCsv( csvData, true, replace, flagsMarkShift4Time);
}

/** Create encoded list of mappings (header: "dm(TTT)V.V" (time TTT, version V.V).)
 * -- "dm(..)V.V" starts plain-text CSV with header row (version V.V).
 * -- "dm^Cxxx" precedes encoded container as salt value.
 * @param xzMap
 * @param cMap
 * @param catFilter
 * @param bFlagsInclude_prefs_trash_stack
 * @return
 */
public synchronized byte[] toCsv( Mapping[] xzMap, int cMap, long catFilter, long bFlagsInclude_prefs_trash_stack) {
	final JResult pooled = JResult.get8Pool();
	catFilter = (0 == catFilter) ? ~0 : catFilter;
	final String ctrb = QValMapSto.string4QVal( Mapping.qhRootOid);
	if (null == xzMap) {
		xzMap = new Mapping[zPrefs.size() + 1000];
		cMap = 0;
		if (0 == (catFilter & Mapping.Cats.PREF.flag)) {
		} else if (null == Mapping.qRootAddress) {
			xzMap[cMap++] = Mapping.make( ".", Mapping.Cats.PREF, ctrb, "00");
		} else {
			xzMap[cMap++] = Mapping.make( ".", Mapping.Cats.PREF, ctrb,
				StringFunc.hexUtf8( Mapping.qRootAddress, true));
			// For backwards compatibility:
			xzMap[cMap++] = Mapping.make( "email_address", Mapping.Cats.PREF, ctrb,
				StringFunc.hexUtf8( Mapping.qRootAddress, true));
			final byte[] lastId = preference_get( "lastId");
			if ((null != lastId) && (8 < lastId.length)) {
				//				xzMap[ cMap ++ ] =
				Mapping.make( "lastId", Mapping.Cats.PREF, ctrb, StringFunc.hex4Bytes( lastId, true));
			}
		}
	}
	if (0 != (1 & bFlagsInclude_prefs_trash_stack)) {
		for (String key : zPrefs.keySet()) {
			xzMap = (cMap >= xzMap.length) ? Arrays.copyOf( xzMap, 2 * xzMap.length) : xzMap;
			String val = StringFunc.hex4Bytes( zPrefs.get( key), true);
			if (null == val) {
				continue;
			}
			xzMap[cMap++] = Mapping.make( key, Mapping.Cats.PREF, ctrb, val);
		}
	}
	if (0 != (4 & bFlagsInclude_prefs_trash_stack)) {
		for (int iElement = 0; iElement < zcStack; ++iElement) {
			xzMap = (cMap >= xzMap.length) ? Arrays.copyOf( xzMap, 2 * xzMap.length) : xzMap;
			String val = ((null == zStack[iElement]) || (null == zStack[iElement].val)) ? null
				: QValMapSto.string4QVals( zStack[iElement].val);
			int inx = zcStack - iElement - 3;
			String key = (-2 == inx) ? "X" : ((-1 == inx ? "Y" : ((0 == inx ? "Z" : ("Z" + inx)))));
			if ((null == val) || (0 >= val.length())) {
				continue;
			}
			xzMap[cMap++] = Mapping.make( key, Mapping.Cats.VAR, ctrb, val);
		}
	}
	if (0 != (catFilter & Mapping.Cats.VAR.flag)) {
		for (String key : zVariables.keySet()) {
			xzMap = (cMap >= xzMap.length) ? Arrays.copyOf( xzMap, 2 * xzMap.length) : xzMap;
			String val = zVariables.get( key).toString();
			if (null == val) {
				continue;
			}
			xzMap[cMap++] = Mapping.make( key, Mapping.Cats.VAR, ctrb, val);
		}
	}
	String header = new String( Dib2Constants.MAGIC_BYTES, StringFunc.STR256);
	// old: header += "\t" + MiscFunc.toDate4Millis() + ... + '\n';
	header += "(" + MiscFunc.dateShort4Millis() + ')' + Dib2Constants.FILE_STRUC_VERSION_STR
		+ Dib2Root.appShort + Mapping.fieldNames + '\n';
	StringBuilder out = new StringBuilder( 100 * xzMap.length);
	out.append( header);
	for (Mapping entry : xzMap) {
		if (0 >= cMap) {
			break;
		}
		--cMap;
		out.append( entry.toCsvLine( null));
		out.append( '\n');
	}
	long handle = 0;
	long[] ahEntries;
	while (null != (ahEntries = QValMapSto.dump( pooled, Dib2Constants.CSVDB_MAP_INDEX, handle, 4096))) {
		for (long hx : ahEntries) {
			handle = hx;
			String oid = QValMapSto.readKey( Dib2Constants.CSVDB_MAP_INDEX, hx, 1);
			if (null == oid) {
				continue;
			}
			Mapping mpg = new Mapping( QValMapSto.qval4AtomicLiteral( pooled, oid),
				QValMapSto.qvals4Aggreg( Dib2Constants.CSVDB_MAP_INDEX, hx));
			if (((bFlagsInclude_prefs_trash_stack & 2) != 0) || (1 < mpg.timeStamp)) {
				if ((mpg.bCategories & catFilter) != 0L) {
					out.append( mpg.toCsvLine( null));
					out.append( '\n');
				}
			}
		}
		if (4096 > ahEntries.length) {
			break;
		}
	}
	return out.toString().getBytes( StringFunc.STRX16U8);
}

public synchronized Mapping[] toList( long catFilter, int maxSize) {
	final JResult pooled = JResult.get8Pool();
	Mapping[] xzMap = new Mapping[maxSize];
	int count = 0;
	catFilter = (0 == catFilter) ? ~0 : catFilter;
	int size = (4096 > maxSize) ? maxSize : 4096;
	long handle = 0;
	long[] ahEntries;
	while (null != (ahEntries = QValMapSto.dump( pooled, Dib2Constants.CSVDB_MAP_INDEX, handle, size))) {
		for (long hx : ahEntries) {
			if (count >= maxSize) {
				break;
			}
			handle = hx;
			String oid = QValMapSto.readKey( Dib2Constants.CSVDB_MAP_INDEX, hx, 1);
			if (null == oid) {
				continue;
			}
			Mapping mpg = new Mapping( QValMapSto.qval4AtomicLiteral( pooled, oid),
				QValMapSto.qvals4Aggreg( Dib2Constants.CSVDB_MAP_INDEX, hx));
			if (0 != (mpg.bCategories & catFilter)) {
				xzMap[count++] = mpg;
			}
		}
		if ((size > ahEntries.length) || (count >= maxSize)) {
			break;
		}
	}
	return Arrays.copyOf( xzMap, count);
}

//=====
}
