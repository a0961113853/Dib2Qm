// Copyright (C) 2020  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_x;

import java.io.File;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.*;

public enum StoRunner implements QExec.QRunnerIf {
//=====

INSTANCE;

/** For saving and initial loading. */
public static volatile boolean wxSaveRequestedLoad = false;

public static volatile boolean wxSyncRequested = false;

/** 0: initial value, 1: failed load attempt, >1000: done */
public static long qLastSave = 0;

private static String getShortDay() {
	return MiscFunc.dateShort4Millis( MiscFunc.currentTimeMillisLinearized()).substring( 0, 6);
}

private static String findLatest( File dir) {
	File file = null;
	String[] names = dir.list();
	if (null != names) {
		String name = null;
		long time = -1;
		for (String x0 : names) {
			if (x0.matches( Dib2Root.dbFileName.replace( ".", ".*"))
				|| (x0.startsWith( Dib2Root.dbFileName)
					&& (x0.endsWith( ".bak") || x0.endsWith( ".old")))) {
				long x1 = new File( dir, x0).lastModified();
				if (time < x1) {
					time = x1;
					name = x0;
				}
			}
		}
		if (0 < time) {
			file = new File( dir, name);
		}
	}
	return (null == file) ? null : file.getAbsolutePath();
}

public static String check4Load() {
	File dir = Dib2Root.platform.getFilesDir( "main");
	String path = null;
	if (null != dir) {
		File file = (null == Dib2Root.dbFileOptionalPath) ? new File( dir, Dib2Root.dbFileName)
			: new File( Dib2Root.dbFileOptionalPath);
		path = file.isFile() ? file.getAbsolutePath() : null;
		if (null != path) {
			return path;
		}
		path = findLatest( dir);
		if (null != path) {
			return path;
		}
	}
	File oldDir = dir;
	dir = Dib2Root.platform.getFilesDir( "external");
	if ((null != dir) && dir.exists() && !dir.equals( oldDir)) {
		path = findLatest( dir);
	}
	return path;
}

/*synchronized*/
void saveAll( long bFlags_toDownloads_asBak) {
	if (0 == bFlags_toDownloads_asBak) {
		TcvCodec.instance.writePhrase();
	}
	File file = (null == Dib2Root.dbFileOptionalPath) || (0 != bFlags_toDownloads_asBak)
		? new File( Dib2Root.platform.getFilesDir( "main"), Dib2Root.dbFileName)
		: new File( Dib2Root.dbFileOptionalPath);
	String path = file.getAbsolutePath();
	if (file.exists()) {
		File old = new File( path + ((0 != bFlags_toDownloads_asBak) ? ".old" : ".bak"));
		if (old.exists()) {
			old.delete();
		}
		file.renameTo( old);
	}
	CcmSto.instance.save( path, true);
	qLastSave = MiscFunc.currentTimeMillisLinearized();
	if (0 != (1 & bFlags_toDownloads_asBak)) {
		File downloadFolder = Dib2Root.platform.getFilesDir( "external");
		if (null != downloadFolder) {
			String name = getShortDay().substring( 4, 6);
			name = Dib2Root.dbFileName.replace( ".dm", "."
				+ ((0 != (2 & bFlags_toDownloads_asBak)) ? "bak" : name)
				+ ".dm");
			file = new File( downloadFolder, name);
			if ((3 != bFlags_toDownloads_asBak)
				|| ((file.lastModified() + 2 * 3600 * 1000) < MiscFunc.currentTimeMillisLinearized())) {
				path = file.getAbsolutePath();
				CcmSto.instance.save( path, true);
//				zLastBackup = zLastSave;
			}
		}
	}
}

boolean loadData( String path) {
	boolean done = false;
	if (CcmSto.instance.isInitialized()) { // & !force
//		Dib2Root.pathInitDataFile = null;
//		UiDataSto.qSwitches[UiDataSto.SWI_STEP_OR_ACCESS_MINUTE] = 999;
		return true;
	}
	// Check if proper phrase is available:
	final boolean dummy = !TcvCodec.instance.setHexPhrase( null);
	final byte[] pass = TcvCodec.instance.getPassFull();
	//	if ((100 > qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ]) && !CsvCodec0.setDummyPhrase( false )) {
	if (null != pass) {
		File dir = Dib2Root.platform.getFilesDir( "main");
		if (null == path) {
			File file = new File( dir, Dib2Root.dbFileName);
			path = file.isFile() ? file.getAbsolutePath() : null;
		}
		if (null == path) {
			path = findLatest( dir);
		}
		if (null != path) {
			done = (0 <= CcmSto.instance.load( path));
			if (done || (dummy && new File( path).exists())) {
				// Do not pull old data if password had been set.
			} else if (new File( path + ".bak").isFile()) {
				done = done || (0 <= CcmSto.instance.load( path + ".bak"));
				done = done || (0 <= CcmSto.instance.load( path + ".old"));
			}
		} else {
			dir = Dib2Root.platform.getFilesDir( "external");
			if ((null != dir) && (dir.exists())) {
				path = findLatest( dir);
				if (null != path) {
					done = done || (0 <= CcmSto.instance.load( new File( path).getAbsolutePath()));
				}
			}
		}
	}
	if (1000 >= qLastSave) {
		qLastSave = done ? 1001 : 1;
		if (done && (Dib2Root.ui.feederCurrent != Dib2Root.ui.feederMain)) {
			Dib2Root.ui.feederCurrent.get().switchFeeder( 1);
		}
	}
	if (done) {
//		Dib2Root.pathInitDataFile = null;
//		UiDataSto.qSwitches[UiDataSto.SWI_STEP_OR_ACCESS_MINUTE] = 999;
		if (!dummy) {
			File downloadFolder = Dib2Root.platform.getFilesDir( "external");
			if (null != downloadFolder) {
				String name = getShortDay().substring( 4, 6);
				name = Dib2Root.dbFileName.replace( ".dm", ".bak.dm");
//				zLastBackup = 1 + new File( downloadFolder, name).lastModified();
			}
		}
	}
	return done;
}

@Override
public boolean requested() {
	return wxSaveRequestedLoad || wxSyncRequested;
}

@Override
public int prepare( long... parameters) {
	return (wxSaveRequestedLoad || wxSyncRequested) ? 1 : 0;
}

private int step_feed = 0;

@Override
public int step() {
	Dib2Root.ui.progress = wxSaveRequestedLoad ? Dib2Lang.kFeedLoadSave[Dib2Root.ui.iLang]
		: Dib2Lang.kFeedWait4Proc[Dib2Root.ui.iLang];
	if (0 >= step_feed) {
		step_feed = 1;
		Dib2Root.platform.invalidate();
	}
	if (!MainThreads.INSTANCE.pauseAllOrRelease4StoRunner( false, MainThreads.wxExitRequest)) {
		return 1;
	}
	UiDataSto.freeze( null);
	if (1 >= step_feed) {
		step_feed = 2;
		Dib2Root.platform.invalidate();
	}
	wxSyncRequested = false;
	if (wxSaveRequestedLoad) {
		wxSaveRequestedLoad = false;
		if ((0 != qLastSave) || CcmSto.instance.isInitialized()) {
			saveAll( 0);
		} else {
			String path = check4Load();
			if (null != path) {
				loadData( path);
			}
		}
	}
	Dib2Root.ui.progress = null;
	step_feed = 0;
	MainThreads.INSTANCE.prepareFeed();
	MainThreads.INSTANCE.pauseAllOrRelease4StoRunner( true, false);
	Dib2Root.platform.invalidate();
	return 0;
}

//=====
}
