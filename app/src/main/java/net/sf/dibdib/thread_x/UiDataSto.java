// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_x;

import static net.sf.dibdib.generic.CcmTemplates.ColorNmz.*;
import static net.sf.dibdib.thread_any.StringFunc.*;

import io.github.gxworks.joined.Rfc1345;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.QValFunc.QVal;
import net.sf.dibdib.thread_any.StringFunc;

/** Variables and helpers: to be written on UI thread,
 * read-only for other threads (not enforced).
 * _D: derived, _R: read-only (fixed/ external value)
 */
public enum UiDataSto {
//=====

UI_READY( 0),

// DEPR
UI_PX_SHIFT_MIN( 0), //-5),
UI_PX_SHIFT_MAX( 24),

UI_FONT_SIZE_PT10( 12 << Dib2Constants.UI_PT10_SHIFT),
UI_FONT_SIZE_FRAME_PT10( 12 << Dib2Constants.UI_PT10_SHIFT),
UI_LINE_SPACING_PT10( Dib2Constants.UI_DSPL_NMZ_LF),

// One or several sheets per slide ...
//TODO
UI_PAGE_HEIGHT( 0),
UI_PAGE_WIDTH( 0),
UI_SHEET_HEIGHT( 0), // Partial page or several pages ...
UI_SHEET_WIDTH( 0),

// Display showing part of board with data and sheets ...
UI_DISPLAY_HEIGHT( Dib2Constants.UI_DSPL_SIZE_MIN_PT10),
UI_DISPLAY_WIDTH( Dib2Constants.UI_DSPL_SIZE_MIN_PT10),
UI_BOARD_HEIGHT( 3 * Dib2Constants.UI_DSPL_INIT_PAGE / 2),
UI_BOARD_WIDTH( Dib2Constants.UI_DSPL_INIT_PAGE),
//Title, status, tools, textField:
UI_DISPLAY_BARS_R( 4),
UI_DISPLAY_BAR_HEIGHT( 9 * UI_LINE_SPACING_PT10.nInit / 8),
UI_DISPLAY_OFFS_CANVAS_X( 0), // on top of margin
UI_DISPLAY_OFFS_CANVAS_Y( 0),
UI_DISPLAY_SPLIT_CANVAS_X( Dib2Constants.UI_DSPL_INIT_SPLIT_X),
UI_DISPLAY_SPLIT_CANVAS_Y( Dib2Constants.UI_DSPL_INIT_SPLIT_Y),
UI_DISPLAY_SPLIT_CANVAS_GAP_X( Dib2Constants.UI_DSPL_NMZ_TAB / 2),
UI_DISPLAY_SPLIT_CANVAS_GAP_Y( 0),

// 2 = 200%, -1 = 75%, ...
UI_ZOOMLVL_DISPLAY( 0),
UI_ZOOMLVL_CANVAS( 0),

UI_FILTER_CATS( 0),

UI_DATA_STO_SIZE_D( QValMapSto.NIL),
;

public final int nInit;
public final QVal vInit;

/////

public static Snap MAIN = new Snap();

/////

private static void init( Snap ctx) {
	for (UiDataSto el : values()) {
		ctx.ints[el.ordinal()] = el.nInit;
		ctx.vals[el.ordinal()] = el.vInit;
	}
}

public static void reset( Snap ctx) {
	UI_DISPLAY_OFFS_CANVAS_X.prep( 0, ctx);
	UI_DISPLAY_OFFS_CANVAS_Y.prep( 0, ctx);
	UI_ZOOMLVL_CANVAS.prep( 0, ctx);
	UI_DISPLAY_SPLIT_CANVAS_GAP_X.prep( Dib2Constants.UI_DSPL_NMZ_TAB / 2, ctx);
	UI_DISPLAY_SPLIT_CANVAS_GAP_Y.prep( 0, ctx);
}

private static void consolidate( Snap ctx) {
	final int maxZoom = Dib2Root.platform.getInitialPx4Pt10ShiftVal() << 1;
	if (0 == UI_READY.i32( ctx)) {
		return;
	}
	if (UI_BOARD_HEIGHT.i32( ctx) < UI_DISPLAY_HEIGHT.i32( ctx)) {
		ctx.ints[UI_BOARD_HEIGHT.ordinal()] = UI_DISPLAY_HEIGHT.i32( ctx);
	}
	if (UI_BOARD_WIDTH.i32( ctx) < UI_DISPLAY_WIDTH.i32( ctx)) {
		ctx.ints[UI_BOARD_WIDTH.ordinal()] = UI_DISPLAY_WIDTH.i32( ctx);
	}
	final int minRatio1 = UI_BOARD_HEIGHT.i32( ctx) / UI_DISPLAY_HEIGHT.i32( ctx);
	final int minRatio2 = UI_BOARD_WIDTH.i32( ctx) / UI_DISPLAY_WIDTH.i32( ctx);
	int maxRatio = (minRatio1 > minRatio2) ? minRatio1 : minRatio2;
	int minZoom = 0;
	for (; maxRatio > 0; maxRatio >>= 1) {
		minZoom -= 2;
	}
	if (UI_ZOOMLVL_CANVAS.i32( ctx) < minZoom) {
		ctx.ints[UI_ZOOMLVL_CANVAS.ordinal()] = minZoom;
	}
	if (UI_ZOOMLVL_CANVAS.i32( ctx) > maxZoom) {
		ctx.ints[UI_ZOOMLVL_CANVAS.ordinal()] = maxZoom;
	}
}

/////

public static int qcKeys4Win = 7;
public static int keys_UniBlock_Offset = 0x2200;
public static int keys_UniBlock_Current = keys_UniBlock_Offset;
public static int keys_UniBlock_FromPad = 1;

public static final int[] kBarTitle = {
	// LEFT
	RED_NEON.argb, ESCAPE, BLUEBLUE.argb, ZOOM_OUT, BLUEBLUE.argb, ZOOM_IN, //"", " ", 
	BLUEBLUE.argb, 'A',
	// CENTER
	//	"", " ", "", "Dib2x", "", "", "", "", //"", "",
	// RIGHT
	RED_NEON.argb, XCUT, GREEN.argb, XPASTE, BLUEBLUE.argb, XCOPY, //"", " ", 
	BLACK.argb, '=',
};

public static final String[] kBarTools = {
	// Fixed position:
	"LANG", "VW  ", "QFIL", "  GO",
	// Flexible:
	" CLR", "SWAP", "STQ", "RCQ", "RCX", " ADD", "SUB ", "MUL ", "DIV ",
};

/////

public static final char[] kKeys_0 = {
	'ä', 'ê', MOVE_UP, SCROLL_UP, 'ë', 'ü', BACKSP,
	'f', 'g', 'ç', 'r', 'l', '/', ' ',
	MOVE_LEFT, 'à', 'ô', 'é', 'û', 'î', ' ',
	SCROLL_LEFT, 'd', 'h', 't', 'n', 'ß', SCROLL_RIGHT,
	TAB, 'â', 'ö', 'è', 'ù', 'ï', MOVE_RIGHT,
	SHIFT, 'b', 'm', 'w', 'v', 'Z', PSHIFT,
	ALT, ',', ' ', SCROLL_DOWN, MOVE_DOWN, '.', CR,
};

public static final char[] kKeys_1 = {
	'_', '{', MOVE_UP, SCROLL_UP, '&', '}', BACKSP,
	'/', '[', '%', '0', '=', ']', '!',
	MOVE_LEFT, '(', '1', '2', '3', ')', '-',
	SCROLL_LEFT, '<', '4', '5', '6', '>', SCROLL_RIGHT,
	TAB, XCUT, '7', '8', '9', '0', MOVE_RIGHT,
	SHIFT, XPASTE, '*', '0', '#', '+', PSHIFT,
	ALT, XCOPY, ' ', SCROLL_DOWN, MOVE_DOWN, '.', CR,
};

public static final char[] kKeys_1_calc = {
	'_', '{', '\u2228', SCROLL_UP, '&', '}', BACKSP,
	'/', '[', '%', /*'\u22bb'*/'\u2207', '!', ']', '\u00b1',
	/*'\u215f'*/'\u00b9', '(', '1', '2', '3', ')', '-',
	SCROLL_LEFT, '\u00ab', '4', '5', '6', '\u00bb', SCROLL_RIGHT,
	'A', 'B', '7', '8', '9', 'E', 'F',
	SHIFT, 'C', '*', '0', '#', '+', PSHIFT,
	ALT, 'D', ' ', SCROLL_DOWN, '\u2206', '.', CR,
};

public static final char[] kKeys_a_Dvorak = {
	'_', '\'', MOVE_UP, SCROLL_UP, 'p', 'y', BACKSP,
	'/', 'f', 'g', 'c', 'r', 'l', '?',
	MOVE_LEFT, 'a', 'o', 'e', 'u', 'i', '-',
	SCROLL_LEFT, 'd', 'h', 't', 'n', 's', SCROLL_RIGHT,
	TAB, '!', 'q', 'j', 'k', 'x', MOVE_RIGHT,
	SHIFT, 'b', 'm', 'w', 'v', 'z', PSHIFT,
	ALT, ',', ' ', SCROLL_DOWN, MOVE_DOWN, '.', CR,
};

public static final char[] kKeys_a = kKeys_a_Dvorak.clone();

public static final char[] kKeys_A_Dvorak = {
	'_', '"', MOVE_UP, SCROLL_UP, 'P', 'Y', BACKSP,
	'/', 'F', 'G', 'C', 'R', 'L', '!',
	MOVE_LEFT, 'A', 'O', 'E', 'U', 'I', '-',
	SCROLL_LEFT, 'D', 'H', 'T', 'N', 'S', SCROLL_RIGHT,
	TAB, ':', 'Q', 'J', 'K', 'X', MOVE_RIGHT,
	SHIFT, 'B', 'M', 'W', 'V', 'Z', PSHIFT,
	ALT, ',', ' ', SCROLL_DOWN, MOVE_DOWN, '.', CR,
};

public static final char[] kKeys_A = kKeys_A_Dvorak.clone();
public static final char[] keys_UnicodeSel = kKeys_A_Dvorak.clone();

public static final char[] kKeys_Z = {
	'Ä', 'Ê', MOVE_UP, SCROLL_UP, 'Ë', 'Ü', BACKSP,
	'F', 'G', 'Ç', 'R', 'L', '/', ' ',
	MOVE_LEFT, 'À', 'Ô', 'É', 'Û', 'Î', ' ',
	SCROLL_LEFT, 'D', 'H', 'T', 'N', 'ß', SCROLL_RIGHT,
	TAB, 'Â', 'Ö', 'È', 'Ù', 'Ï', MOVE_RIGHT,
	SHIFT, 'B', 'M', 'W', 'V', 'Z', PSHIFT,
	ALT, ',', ' ', SCROLL_DOWN, MOVE_DOWN, '.', CR,
};

public static final char[][] kPadKeys_Dvorak = { kKeys_0, kKeys_1, kKeys_a_Dvorak, kKeys_A_Dvorak, kKeys_Z };
public static final char[][] kPadKeys_Calc = { kKeys_0, kKeys_1_calc, kKeys_a, kKeys_A, keys_UnicodeSel };

public static char[][] qPadKeys = kPadKeys_Calc;

static {
	char cA = 'Z';
	char ca = 'z';
	for (int i0 = kKeys_a.length - 1; (i0 >= 0) && ('A' <= cA); --i0) {
		kKeys_A[i0] = ('A' <= kKeys_A[i0]) ? (cA--) : kKeys_A[i0];
		kKeys_a[i0] = ('A' <= kKeys_a[i0]) ? (ca--) : kKeys_a[i0];
	}
	setUnicodeBlock( keys_UniBlock_Current, 0);
}

//////

/** Iterative snapshots for context containers.
 */
//=====
public static final class Snap {
//=====
private final int[] ints;
private final QVal[] vals;
private final Snap prep;
private boolean changed;

private Snap( int[] ints, QVal[] vals) {
	this.ints = ints;
	this.vals = vals;
	prep = null;
	changed = false;
	//	consolidate( this);
}

private Snap() {
	ints = new int[1 + UI_DATA_STO_SIZE_D.ordinal()];
	vals = new QVal[1 + UI_DATA_STO_SIZE_D.ordinal()];
	init( this);
	prep = new Snap( ints.clone(), vals.clone());
	changed = false;
}

private Snap( Snap old) {
	ints = old.ints;
	vals = old.vals;
	consolidate( this);
	prep = new Snap( ints.clone(), vals.clone());
	changed = false;
}
//=====
}

private UiDataSto( int i32) {
	nInit = i32;
	vInit = QValMapSto.NIL;
}

private UiDataSto( QVal val) {
	nInit = -1;
	vInit = val;
}

private UiDataSto( String val) {
	final JResult pooled = JResult.get8Pool();
	vInit = QValMapSto.qval4AtomicLiteral( pooled, val);
	nInit = -1;
}

public int i32( Snap ctx) {
	ctx = (null != ctx) ? ctx : UiDataSto.MAIN;
	return ctx.ints[ordinal()];
}

public long i64( Snap ctx) {
	ctx = (null != ctx) ? ctx : UiDataSto.MAIN;
	return (QValMapSto.NIL == ctx.vals[ordinal()]) ? ctx.ints[ordinal()]
		: (long) (QValMapSto.doubleD4oQVal( ctx.vals[ordinal()]) / 1.0e4);
}

public QVal val( Snap ctx) {
	ctx = (null != ctx) ? ctx : UiDataSto.MAIN;
	return (QValMapSto.NIL == ctx.vals[ordinal()]) && (-1 != ctx.ints[ordinal()])
		? QValMapSto.qval4DoubleD4( ctx.ints[ordinal()] * 1.0e4)
		: ctx.vals[ordinal()];
}

public int prep( int i32, Snap ctx) {
	ctx = (null != ctx) ? ctx : UiDataSto.MAIN;
	ctx.changed = true;
	ctx.prep.ints[ordinal()] = i32;
	return ctx.ints[ordinal()];
}

public QVal prep( QVal xmVal, Snap ctx) {
	ctx = (null != ctx) ? ctx : UiDataSto.MAIN;
	ctx.changed = true;
	ctx.prep.vals[ordinal()] = xmVal;
	return val( ctx);
}

public static void prepReset( Snap ctx) {
	ctx = (null != ctx) ? ctx : UiDataSto.MAIN;
	ctx.changed = true;
	init( ctx.prep);
}

public static boolean prep( String name, QVal val, Snap ctx) {
	ctx = (null != ctx) ? ctx : UiDataSto.MAIN;
	ctx.changed = true;
	if (!name.equals( StringFunc.toUpperCase( name)) || !name.equals( StringFunc.toLowerCase( name))) {
		if (!name.contains( "_")) {
			name = name.replaceAll( "[A-Z]", "_$0").replaceFirst( "$_", "");
		}
	}
	name = StringFunc.toUpperCase( name);
	if (name.endsWith( "_D") || name.endsWith( "_R")) {
		return false;
	}
	try {
		UiDataSto var = valueOf( name);
		if (QValMapSto.NIL == ctx.vals[var.ordinal()]) {
			var.prep( (int) (QValMapSto.doubleD4oQVal( val) / 1.0e4), ctx);
		} else {
			var.prep( val, ctx);
		}
	} catch (Exception e) {
		return false;
	}
	return true;
}

/** To be called from StoRunner (sync) or UI thread if nothing had been triggered.
 * @param ctx
 * @return
 */
public static Snap freeze( Snap ctx) {
	ctx = (null == ctx) ? MAIN : ctx;
	if (!MainThreads.INSTANCE.isIdle()) {
		return ctx;
	}
	if (null == ctx) {
		return new Snap();
	} else if (null == ctx.prep) {
		return new Snap( ctx);
	}
	//TODO Have conflicting writes to ctx.prep based on timestamp ...
	return ctx.changed ? new Snap( ctx.prep) : ctx;
}

/////////

private static int setUnicodeBlock_perRound = 0;

public static int setUnicodeBlock( int block, int rounds) {
	block = (0 > block) ? keys_UniBlock_Current : block;
	if (0 >= setUnicodeBlock_perRound) {
		int tot = 0;
		for (int i0 = 0; i0 < keys_UnicodeSel.length; ++i0) {
			tot += (kKeys_A_Dvorak[i0] > '.') ? 1 : 0;
		}
		setUnicodeBlock_perRound = tot;
	}
	keys_UniBlock_Current = (block + rounds * setUnicodeBlock_perRound) & 0xffff;
	if (0xd800 <= keys_UniBlock_Current) {
		keys_UniBlock_Current = 0x2000;
	}
	char c0 = (char) (keys_UniBlock_Current + setUnicodeBlock_perRound);
	for (int i0 = keys_UnicodeSel.length - 1; i0 >= 0; --i0) {
		keys_UnicodeSel[i0] = (kKeys_A_Dvorak[i0] > '.') ? (--c0) : kKeys_A_Dvorak[i0];
	}
	keys_UniBlock_FromPad = 1;
	return keys_UniBlock_Current;
}

public static void setUnicodeBlockOffset( String blockNameOrOffset) {
	int block = (int) StringFunc.long4String( blockNameOrOffset, -1);
	if (0 > block) {
		for (int i0 = 1; i0 < Rfc1345.kUnicodeBlocks.length; i0 += 2) {
			if (Rfc1345.kUnicodeBlocks[i0].contains( blockNameOrOffset)) {
				block = Rfc1345.kUnicodeBlocks[i0 - 1].charAt( 0);
			}
		}
	}
	if (0 <= block) {
		keys_UniBlock_Offset = setUnicodeBlock( block, 0);
	}
}

//=====
}
