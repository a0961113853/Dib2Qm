// Copyright (C) 2016,2019,2020  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.config;

import net.sf.dibdib.thread_any.StringFunc;

public interface Dib2Constants {
//=====

public static final String[] NO_WARRANTY = new String[] { "ABSOLUTELY NO WARRANTY", "-- see license!" };

public static final String[] LICENSE_LIST = new String[] {
	"/license.txt",
	"/spongycastle_license.txt",
	"/javamail_license.txt",
	"/apache_license2.txt",
};

/**
 * Version myysb :=> major '.' (year/branch | 1) + (stable ? 1 : 0)) '.' (step * 10 + build variant/patch)
 */
public static final int VERSION = 2275; // alpha, 0.22.75
public static final String VERSION_STRING = "0" + ("" + VERSION).replaceAll( "..", ".$0").replace( ".00", "");
public static final int RFC4880_EXP2 = 0x80 | 0x40 | 62; // 0xfe
public static/*final*/byte FILE_STRUC_VERSION = 62; // 50; // Actual version
public static final byte FILE_STRUC_VERSION_CMPAT = 60; // 50; // Matching lower version, for header
public static/*final*/byte FILE_STRUC_VERSION_MIN = 60; // 50; Minimum supported
public static final String FILE_STRUC_VERSION_STR = "" + (FILE_STRUC_VERSION / 100) + '.' + (FILE_STRUC_VERSION % 100);

/** 'dm(' + date + ')' + version for CSV/TSV list (plain),
 * YYXXX (hex) as id for single TSV row,
 * "dmM(" with "...)" + version for mnemonic encoding.
 * RFC4880_EXP2 + len + ... for compressed (message) data.
 * Zipped CSV/TSV data:
 * RFC4880_EXP2 + len + 'dm' + 'z' + stamp (+'x').
 * Encoded TSV data, version 0.62 (with reversed TLVs/ TCVs):
 * RFC4880_EXP2 + len + 'dm' + cipher ('C') + algo ('A') + reqVersion*100 (62) + version*100 (>=62)
 * + salt iteration mag (it = (1 << mag) + 3)
 * + TC1 (0xa7) + senderAddress/Hash + TC2 + senderPK/Hash + TC3 + otherPK/Hash + TC4
 * + IV + TC5 + data + TC6 + sign/SIV + TC7 + 0x97.
 * Encoded data, version 0.6: (same as 0.5, but indicates support of 0.7x).
 * Encoded data, version 0.5:
 * RFC4880_EXP2 + len + 'dm' + version + algo
 * + block count of header (salt/IV with key info + address + additional key data)
 * + block count of appended signature.
 *  */
public static final byte[] MAGIC_BYTES = "dm".getBytes( StringFunc.STR256);

long TIME_MIN_2017_01_01 = 1483228800000L;
long TIME_MAX = 2 * TIME_MIN_2017_01_01;

public static final String PASS_FILENAME = "DibdibP.txt";
public static final String PASS_FILENAME_X = "DibdibX.dm";
public static final int SALT_ITERATION_MAG = 10; // 10:(1<<10)+3, -1:1174;
public static final long SAVE_INTERNAL = 10 * 60 * 1000;
public static final long MAX_DELTA_ACCESS_CHECK = 18 * 3600 * 1000; // 18 h
public static final int INIT_LINES_FILTER = 80;
public static final String ERROR_Str = "ERROR";
public static final int SHASH_MAX = 32; // up to 30 chars or truncated as 31 chars or 21 chars + SHA1
//public static final int MAPPING_KEY_TYPES = 2; // shash (1) for String, shash + OID (1+1) for Mapping
//public static final int MAPPING_KEY_TYPE_OID = 1;
/** For QMap: Dedicated lists with OID + mapping data */
public static final int CSVDB_MAP_INDEX = 1;

public static final String DATA_DEFAULT_ID__0 = "0";
//public static final String[] SOURCES_DEFAULT = { "n.n.", "dm", "n.n." };
public static final String[] SOURCES_DEFAULT = { "0", "0", "0" };
public static final long TIME_SHIFTED = 3L;
/** Use bit 1 of msec value to indicate bad or unaligned clock value. */
public static final long TIME_SHIFTED_UNKNOWN = 2L;
/** Use bit 0 of msec value to indicate bad or unaligned time zone value. */
public static final long TIME_SHIFTED_HOUR = 1L;
public static final int MAXVIEW_MSGS_INIT = 15;

public static final int UI_DSPL_SIZE_MIN_PX = 240;
public static final int UI_DSPL_SIZE_MIN_INCH = 2;
public static final int UI_PT_P_INCH = 72;
public static final int UI_PT10_SHIFT = 10;
public static final int UI_PT10_P_INCH = UI_PT_P_INCH << UI_PT10_SHIFT;
public static final int UI_DSPL_SIZE_MIN_PT10 = (UI_DSPL_SIZE_MIN_INCH * UI_PT_P_INCH) << UI_PT10_SHIFT;
public static final int UI_DSPL_NMZ_TAB = (8 * 8) << UI_PT10_SHIFT;
public static final int UI_DSPL_NMZ_LF = 16 << UI_PT10_SHIFT; // 14
public static final int UI_TOUCH_TOLERANCE = (UI_PT10_P_INCH / 2);
public static final int UI_DSPL_INIT_MARGIN = 3 << UI_PT10_SHIFT;
public static final int UI_DSPL_INIT_SPLIT_X = 3 * UI_DSPL_NMZ_TAB / 8;
public static final int UI_DSPL_INIT_SPLIT_Y = 3 * UI_DSPL_NMZ_LF + 2 * UI_DSPL_INIT_MARGIN;
public static final int UI_DSPL_INIT_PAGE = 8 * UI_PT10_P_INCH;

public static final int UI_FRAME_BARS = 4;
public static final int UI_FRAME_BAR_ITEMS_PER_SIDE = 4; //5
public static final int UI_FRAME_BAR_TITLE = 0;
public static final int UI_FRAME_BAR_TOOLS = 1;
public static final int UI_FRAME_BAR_ENTRY = 2;
public static final int UI_FRAME_BAR_STATUS = 3;

///// 'Normalized' font metrics, fraction of height h/256

public static final int UI_FONT_NMZ_SHIFT = 8; // 256th
public static final int UI_FONT_NMZ_HEIGHT = 256; // h = a+d
public static final int UI_FONT_NMZ_SIZE = 320; // g = h + leading (x/2)
public static final int UI_FONT_NMZ_CAP_H = 181; // a = sqrt(2) * x
public static final int UI_FONT_NMZ_X_HEIGHT = 128; // x = h/2
public static final int UI_FONT_NMZ_DESCENT = 75; // d = 2x - a; a+d = 2x
public static final int UI_FONT_NMZ_M_ADV = 240; // m
public static final int UI_FONT_NMZ_N_WIDTH = 128; // x
public static final int UI_FONT_NMZ_N_ADV = 140; // avg
public static final int UI_FONT_NMZ_MONO_ADV = 160;
public static final int UI_FONT_NMZ_MONO_MAX = 180;

///// DEPR

public static final int UI_X_WIN_X_MARGIN = 8;

//=====
}
