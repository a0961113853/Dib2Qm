// Copyright (C) 2016,2017,2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.config;

import java.util.Locale;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.PlatformFunc.PlatformFuncIf;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_ui.UiPres;
import net.sf.dibdib.thread_x.QValMapSto;

public class Dib2Root implements Dib2Constants {
//=====

///// Fixed values:

public static final Locale locale = null; // null for multilingual texts with CLDR (UCA DUCET) sorting

///// To be initialized:

public static String appShort = "";
public static char platformMarker = 0;
public static PlatformFunc.PlatformFuncIf platform;

public static String dbFileName = null;
public static String dbFileOptionalPath = null;

/** First one is default for encoding. */
public static TsvCodecIf[] codecs = new TsvCodecIf[] {
	CodecFunc.instance,
};

///// Overall state/ feedback

public static Descript.Ui ui = new Descript.Ui();

//=====
public static enum CmdLineArgs {
//=====

HELP( "h?", "Print help."),
WIDTH( "", "Width of window/ frame, e.g. '--width=320'."),
HEIGHT( "", "Height of window/frame, e.g. '--height=480'."),
ZOOM( "", "Zoom level, e.g. '--zoom=2'."),
//READONLY("r", "Read-only mode."),
TTY( "t", "Switch to simple text mode/ terminal mode - no GUI."),
CARRIAGERETURN( "", "Text mode using CR char's - for some special terminals."),
VERSION( "v", "Print version info."),
DEBUG( "d", "Switch to debug mode - in case of errors."),
X0( null, "Path of encoded data file."),
X1( null, null),
X2( null, null),
;

public final String abbrev;
public final String descr;
public String value;

private CmdLineArgs( String abbrev, String descr) {
	this.abbrev = abbrev;
	this.descr = descr;
	value = null;
}

//=====
}

/** General initialization here, specific values at caller. */
public static void init( char xPlatformMarker, String xModuleShort, String xDbFileName,
	PlatformFuncIf xPlatform, TsvCodecIf[] xmCsvCodecs) {
	appShort = xModuleShort;
	dbFileName = xDbFileName;
	platform = xPlatform;
	codecs = (null == xmCsvCodecs) ? codecs : xmCsvCodecs;
	QValMapSto.aggregations[CSVDB_MAP_INDEX] = new QValMapSto( Mapping.kcKeyTypes);
	if (0 != platformMarker) {
		return;
	}
	platformMarker = xPlatformMarker;
	MiscFunc.timeZoneDone = false;
	TcvCodec.instance.init( platformMarker);
	MainThreads.INSTANCE.init( (null != CmdLineArgs.TTY.value) || (null != CmdLineArgs.CARRIAGERETURN.value));
	UiPres.INSTANCE.init();
}

/** Enable early logging as much as possible */
public static void log( String... aMsg) {
	//	MiscFunc.keepLog( aMsg[ 0 ], aMsg[ 1 ] );
	if (null != platform) {
		platform.log( aMsg);
	}
}

public static void scanArgs( String[] args) {
	CmdLineArgs[] aRegular = { CmdLineArgs.X0, CmdLineArgs.X1, CmdLineArgs.X2 };
	int iRegular = 0;
	String abbrev = "";
	for (int i0 = 0; i0 < args.length; ++i0) {
		final String arg = args[i0].toUpperCase( Locale.ROOT);
		int offs = (arg.startsWith( "--") ? 2
			: ((arg.startsWith( "-") || arg.startsWith( "/"))) ? 1 : 0);
		if (0 >= offs) {
			if (iRegular < aRegular.length) {
				aRegular[iRegular++].value = arg;
			}
			continue;
		}
		for (CmdLineArgs opt : CmdLineArgs.values()) {
			String option = opt.name();
			int ix;
			if (arg.substring( offs).startsWith( option)) {
				if ((0 < (ix = arg.indexOf( ':'))) || (0 < (ix = arg.indexOf( '=')))) {
					opt.value = arg.substring( ix + 1);
					offs = -1;
					break;
				}
				opt.value = "";
				offs = -1;
				break;
			}
		}
		if (1 == offs) {
			abbrev += arg.substring( 1);
		}
	}
	if (0 < abbrev.length()) {
		for (char flag : abbrev.toCharArray()) {
			for (CmdLineArgs opt : CmdLineArgs.values()) {
				if ((0 <= opt.abbrev.indexOf( flag)) && (null == opt.value)) {
					opt.value = "";
				}
			}
		}
	}
}

public static String getVersionInfo( String module) {
	String out = module + " version" + VERSION_STRING + '\n';
	out += Dib2Constants.NO_WARRANTY[0] + ' ' + Dib2Constants.NO_WARRANTY[1] + '\n';
	return out;
}

public static String getHelp( String module) {
	StringBuilder out = new StringBuilder( 200 + 64 * CmdLineArgs.values().length);
	out.append( getVersionInfo( module));
	out.append( "\n" + "Usage: Call this program with the following arguments/ options:\n");
	for (CmdLineArgs opt : CmdLineArgs.values()) {
		if (null == opt.abbrev) {
			if (null != opt.descr) {
				out.append( "ARG" + "  \t" + opt.descr + '\n');
			}
			continue;
		}
		out.append( (0 >= opt.abbrev.length()) ? "" : ("-" + opt.abbrev.charAt( 0) + ", "));
		out.append( "--" + opt.name().toLowerCase( Locale.ROOT) + "  \t");
		out.append( opt.descr + '\n');
	}
	return out.toString();
}

//=====
}
