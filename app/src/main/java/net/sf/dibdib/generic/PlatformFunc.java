// Copyright (C) 2020,2021  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.generic;

import java.io.*;
import net.sf.dibdib.config.Dib2Constants;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_ui.UiPres;
import net.sf.dibdib.thread_wk.WkRunner;

/** Partial implementation of the following If, also needed as stand-alone class.
 */
public class PlatformFunc {
//=====

//=====
public static interface PlatformFuncIf {
//=====

void log( String... aMsg);

void invalidate();

/** Proper directory depending on platform.
* @param parameters "main" or "external" (=download) or "safe".
* @return null if nothing suitable found (e.g. for "safe").
*/
File getFilesDir( String... parameters);

String[] getLicense( String xAdditionalVersionInfo, String... resources);

int getDisplayHeight();

int getDisplayWidth();

int getInitialPx4Pt10ShiftVal();

boolean pushClipboard( String label, String text);

String getClipboardText();

void toast( String msg);

//=====
}

/////

public static char[] kLines = {
	' ', '-', '|', '+',
	//' ', '\u2500', '\u2502', '\u253c',
	'\\', '\u00ac', '$', '&',
	'/', '\u00a5', 'Y', '#',
	'X', '\u00a5', '$', '#' };

public static char[] kBoxes = {
	' ', // 0
	'\u2580', // 1 upper
	'\u2584', // 2 lower
	'\u2588', // 3 both
};

public void keyTyped( char k) {
	if (' ' <= k) {
		UiPres.INSTANCE.handleUiEventChar( k);
	} else { // if ('\t' != k) {
		QValToken cmd = UiPres.INSTANCE.handleUiEventCtrl4UiThread( k);
		if (null != cmd) {
			WkRunner.INSTANCE.wxGateIn.push( cmd);
			MainThreads.INSTANCE.triggerWk();
		} else {// if (MainThreads.INSTANCE.isIdle()) {
			//UiDataSto.MAIN = UiDataSto.freeze( null);
			MainThreads.INSTANCE.triggerSync();
		}
	}
}

/** Not as easy as it should be ... :-)
 * @param parameters empty='main'/ 'safe'/ 'external'(=downloads for regular env)/ 'tmp'/ 'home'.
 * @return File, based on '.' as fallback.
 */
public File getFilesDir( String... parameters) {
	try {
		if (0 < parameters.length) {
			final String par = StringFunc.toLowerCase( parameters[0]);
			if (par.contains( "safe")) {
				// Bad luck on PCs :-(
				return null;
			} else if (par.contains( "external") || par.contains( "home")) {
				final String home = System.getProperty( "user.home");
				final File dir = new File( home + (par.contains( "home") ? "" : "/Downloads"));
				if (dir.isDirectory()) {
					return dir;
				}
			} else if (par.contains( "tmp")) {
				final String tmp = System.getProperty( "java.io.tmpdir");
				final File dir = new File( tmp);
				if (dir.isDirectory()) {
					return dir;
				}
			}
		}
	} catch (Exception e) {
	}
	return new File( ".");
}

private String[] getLicense_lines;

public String[] getLicense( String xAdditionalVersionInfo, String... resources) {
	if (null != getLicense_lines) {
		return getLicense_lines;
	}
	resources = ((null == resources) || (0 >= resources.length)) ? Dib2Constants.LICENSE_LIST : resources;
	//	final URL[] urls = new URL[] { getClass().getResource( "/license.txt" ), ...
	String license = "(Version " //,
		+ ((null == xAdditionalVersionInfo) ? "" : (xAdditionalVersionInfo + "/ ")) //,
		+ Dib2Constants.VERSION_STRING + ")\n";
	try {
		for (String rsc : resources) {
			InputStream is;
			if (null == rsc) {
				continue;
			}
			is = new BufferedInputStream( MiscFunc.class.getResourceAsStream( rsc)); //url.openStream();
			license += MiscFunc.readStream( is);
		}
	} catch (Exception e) {
		if (!license.contains( "Could not access")) {
			license = Dib2Constants.NO_WARRANTY[0] + "\n(Could not access license files.)\n\n" + license;
		}
	}
	getLicense_lines = license.split( "\n");
	return getLicense_lines;
}

//=====
}
