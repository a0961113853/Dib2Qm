// Copyright (C) 2020  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.generic;

import net.sf.dibdib.thread_wk.FeederRf;

/** State descriptors */
public final class Descript {
//=====

public static final class Ui {
public boolean bDisclaimerDone = false;
public boolean bExitConfirmation = false;
/** 'View model' ..., to be used by platform's UI. */
public volatile FeederRf feederCurrent = null;
public FeederRf feederMain = null;
public int iLang = 0; // --> Dib2Local.kLanguages
public String progress = null;
public String error = null;
public int cSlides = 1;
public int iSlide = 1;
public int iSlideSupplement = 0;
}

//=====
}
