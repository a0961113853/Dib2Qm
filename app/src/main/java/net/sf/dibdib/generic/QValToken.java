// Copyright (C) 2016,2017,2018,2019,2020  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.generic;

import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_any.QValFunc.QVal;

/** For token flow/ relay model, to be extended (cmp. QDspPrcs and QPlace).
 * Based on Petri net (Arc weight = 1) with typed/ guarded Places for data tokens.
 * (Places impose guards on their preceding Transitions).
 */
public class QValToken {
//=====

/** msec value (0 if trashed), lowest 2 bits may be adjusted according to 'TIME_SHIFTED'. */
public long timeStamp;

/////

public QVal[] val = null;

public QCalc operator;
public char uiKeyOrButton;
public String uiParameter;

public String[] errorTrace = null;
public QValToken[] args4Lazy = null;
public int cArgsZippedTodo = 0;
public QValToken result1 = null;
public QValToken result2 = null;

/////

public static final QValToken EMPTY = new QValToken();

/////

public static QValToken create( QVal... xmVal) {
	QValToken out = new QValToken();
	out.val = xmVal;
	return out;
}

public void setTimeStamp() {
	timeStamp = MiscFunc.currentTimeMillisLinearized();
}

//=====
}
