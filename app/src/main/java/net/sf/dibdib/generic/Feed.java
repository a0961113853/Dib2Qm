// Copyright (C) 2020  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.generic;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;
import net.sf.dibdib.config.Dib2Constants;
import net.sf.dibdib.thread_any.QValFunc.QVal;
import net.sf.dibdib.thread_x.UiDataSto;

public enum Feed {
//=====

///// Context item

POS,
POSX,
POSY,
RBASE,
RMOVE,
LNWIDTH,
LNCAP,
LNJOIN,
DASH,
RGBCOLOR,
FACE,
STYLE,
WEIGHT,
HEIGHT,
//LEADING,
TXLF,
ENTRY,

///// Advancing item

LINE,
ARC,
CURVE,
TEXT,
TXSHLEFT,
TXCTR,
TXBOX,
IMAGE,
//,
;

private static UiScriptContext[] zPoolScriptContext = new UiScriptContext[64];
private static AtomicInteger zcPoolScriptContext = new AtomicInteger( 0);

//=====
public static final class UiScriptContext {
//=====

/** Negative if recycled */
public int eLine;
public int eBasePt10;
public int xPt10;
public int yPt10;
public UiScriptContext prevContext;
public int heightPt10;
public int color;
public int zHeightPx;

private UiScriptContext init( UiScriptContext previousVals, boolean forFrame) {
	final int lineSpacing = forFrame
		? UiDataSto.UI_DISPLAY_BAR_HEIGHT.i32( null)
		: UiDataSto.UI_LINE_SPACING_PT10.i32( null);
	final int height = forFrame ? UiDataSto.UI_FONT_SIZE_FRAME_PT10.i32( null)
		: UiDataSto.UI_FONT_SIZE_PT10.i32( null);
	if (null != previousVals) {
		xPt10 = previousVals.xPt10;
		yPt10 = previousVals.yPt10;
		eLine = previousVals.eLine;
		eBasePt10 = previousVals.eBasePt10;
		prevContext = previousVals.prevContext;
		heightPt10 = previousVals.heightPt10;
		color = previousVals.color;
		return this;
	}
	xPt10 = 0;
	heightPt10 = height;
	eLine = lineSpacing;
	if (forFrame) {
		eBasePt10 = ((lineSpacing + ((heightPt10 * Dib2Constants.UI_FONT_NMZ_CAP_H) //,
		>> Dib2Constants.UI_FONT_NMZ_SHIFT)) >> 1);
	} else {
		eBasePt10 = ((lineSpacing + ((heightPt10 * Dib2Constants.UI_FONT_NMZ_X_HEIGHT) //,
		>> Dib2Constants.UI_FONT_NMZ_SHIFT)) >> 1);
	}
	yPt10 = eBasePt10 - ((heightPt10 * Dib2Constants.UI_FONT_NMZ_CAP_H) >> Dib2Constants.UI_FONT_NMZ_SHIFT);
	eBasePt10 -= yPt10;
	prevContext = previousVals;
	color = CcmTemplates.ColorNmz.BLACK.argb;
	return this;
}

private UiScriptContext() {
	eLine = -1;
//	initParameters( null, -1, false);
}

public UiScriptContext recycleMe() {
	// Not too many ...
	if ((0 <= eLine) && (1024 > zcPoolScriptContext.get())) {
		eLine = -1;
		xPt10 = -1;
		int inx = zcPoolScriptContext.getAndIncrement();
		if (inx >= zPoolScriptContext.length) {
			zPoolScriptContext = Arrays.copyOf( zPoolScriptContext, 2 * zPoolScriptContext.length);
		}
		zPoolScriptContext[inx] = this;
	}
	prevContext = null;
	return null;
}
//=====
}

public static UiScriptContext makeScriptContext( UiScriptContext previous, boolean forFrame) {
	while (0 < zcPoolScriptContext.get()) {
		int inx = zcPoolScriptContext.decrementAndGet();
		if (null != zPoolScriptContext[inx]) {
			return zPoolScriptContext[inx].init( previous, forFrame);
		}
	}
	return new UiScriptContext().init( previous, forFrame);
}

private static Script[] zPoolScript = new Script[64];
private static AtomicInteger zcPoolScript = new AtomicInteger( 0);

//=====
public static final class Script {
//=====

public int cScript = -1;
public Script[] script = null;
public int no = -1;
public Feed tag = null;
public int parX = 0;
public int parY = 0;
public long parN0 = 0;
public String parS0 = null;
public QVal[] parsX = null;

private Script() {
}

public Script recycleMe() {
	// Not too many ...
	if ((0 <= cScript) && (2048 > zcPoolScript.get())) {
		cScript = -1;
		no = -1;
		int inx = zcPoolScript.getAndIncrement();
		if (0 <= inx) {
			if (inx >= zPoolScript.length) {
				zPoolScript = Arrays.copyOf( zPoolScript, 2 * zPoolScript.length);
			}
			zPoolScript[inx] = this;
			if (null != script) {
				for (int i0 = 0; i0 < script.length; ++i0) {
					if (null == script[i0]) {
						break;
					}
					script[i0].recycleMe();
				}
			}
		}
	}
	parsX = null;
	script = null;
	return null;
}
//=====
}

public static Script makeScript( int no) {
	while (0 < zcPoolScript.get()) {
		int inx = zcPoolScript.decrementAndGet();
		if ((0 <= inx) && (null != zPoolScript[inx])) {
			zPoolScript[inx].no = no;
			return zPoolScript[inx];
		}
	}
	Script out = new Script();
	out.no = no;
	return out;
}

public static Script makeScriptEl( int no, Feed tag, int... pars) {
	Script out = makeScript( no);
	out.tag = tag;
	out.parS0 = null;
	if (null == pars) {
		return out;
	}
	if (0 < pars.length) {
		out.parX = pars[0];
	}
	if (1 < pars.length) {
		out.parY = pars[1];
	}
	return out;
}

public static Script makeScriptEl( int no, Feed tag, String par) {
	Script out = makeScript( no);
	out.tag = tag;
	out.parS0 = par;
	return out;
}

public static Script makeScriptEl( int no, Feed tag, String parA, long parB) {
	Script out = makeScript( no);
	out.tag = tag;
	out.parN0 = parB;
	out.parS0 = parA;
	return out;
}

//=====
}
