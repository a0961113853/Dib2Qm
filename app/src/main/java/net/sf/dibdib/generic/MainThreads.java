// Copyright (C) 2020,2021  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.generic;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import net.sf.dibdib.config.Dib2Root;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_ui.UiPres;
import net.sf.dibdib.thread_wk.*;
import net.sf.dibdib.thread_x.StoRunner;

public enum MainThreads {
//=====

INSTANCE;

///// Threaded (in/ out/ trigger)

public static volatile boolean wxExitRequest = false;
private volatile boolean zPauseRequest = false;

/////

private boolean terminalMode = false;

//worker, mouse click handler, storage, background
private static ExecWrapper[] zRunnables = new ExecWrapper[] {
	new ExecWrapper( StoRunner.INSTANCE),
	new ExecWrapper( WkRunner.INSTANCE),
	new ExecWrapper( MouseClickedRepeater.INSTANCE),
};
private static final int cThreadPool = 2 * zRunnables.length;
private static final ExecutorService threadPool = Executors.newFixedThreadPool( cThreadPool);

//=====
public static enum MouseClickedRepeater implements QExec.QRunnerIf {
//=====

INSTANCE;

public static final int maxTime = 3000;
public static final int maxCount = 50;
char key = 0;
long dEnd;

long start;
int maxPerRound;
int tot;

@Override
public boolean requested() {
	return 0 != key;
}

private QValToken handleAsClick( char key) {
	if (' ' <= key) {
		UiPres.INSTANCE.handleUiEventChar( key);
	} else if (0 < key) {
		QValToken cmd = UiPres.INSTANCE.handleUiEventCtrl4UiThread( key);
		if (null != cmd) {
			return cmd;
		}
	}
	return null;
}

@Override
public int prepare( long... parameters) {
	key = (char) parameters[0];
	if (0 == key) {
		return 0;
	}
	start = MiscFunc.currentTimeMillisLinearized();
	dEnd = start + maxTime - 50; //parameters[2] - 50;
	start += 250;
	maxPerRound = 1;
	tot = 0;
	return maxCount - tot;
}

@Override
public int step() {
	if (MainThreads.INSTANCE.zPauseRequest || (0 >= dEnd) || (dEnd <= start)) {
		dEnd = 0;
		return 0;
	}
	if (0 == tot) {
		QValToken cmd = handleAsClick( key);
		tot = 1;
		if (null != cmd) {
			WkRunner.INSTANCE.wxGateIn.push( cmd);
			MainThreads.INSTANCE.triggerWk();
			dEnd = 0;
			return 0;
		}
		return maxCount - tot;
	}
	try {
		Thread.sleep( 50);
	} catch (InterruptedException e) {
		dEnd = 0;
		return 0;
	}
	final long dCur = MiscFunc.currentTimeMillisLinearized();
	int count = maxCount - (int) (dEnd - dCur) * maxCount / (int) (dEnd - start) - tot;
	if (dCur <= start) {
		return maxCount - tot;
	} else if (dEnd <= dCur) {
		dEnd = 0;
		maxPerRound <<= 1;
		count = (maxPerRound >= count) ? count : maxPerRound;
		for (; 0 < count; --count) {
			handleAsClick( key);
		}
		return 0;
	}
	maxPerRound = (((dCur - start) <= 600) || (4 >= tot)) ? 1
		: ((maxPerRound < count) ? (1 + maxPerRound) : maxPerRound);
	count = (maxPerRound >= count) ? count : maxPerRound;
	tot += count;
	for (; 0 < count; --count) {
		handleAsClick( key);
	}
	return maxCount - tot;
}

//=====
}

//=====
private static class ExecWrapper implements Runnable {
//=====

private final AtomicInteger mcReq;
private final QExec.QRunnerIf mExec;
private volatile boolean mPaused = false;

private ExecWrapper( QExec.QRunnerIf exec) {
	this.mcReq = new AtomicInteger( 0);
	this.mExec = exec;
}

@Override
public void run() {
//	INSTANCE.yield4Sync( 0);
	while (true) {
		boolean done = false;
		while (!done) {
			mPaused = MainThreads.INSTANCE.zPauseRequest;
			if (mPaused && (zRunnables[0] != this)) {
				try {
					Thread.sleep( 100);
				} catch (InterruptedException e) {
					done = true;
				}
			} else {
				done = 0 >= mExec.step();
			}
		}
		if (0 >= mcReq.decrementAndGet()) {
			break;
		}
	}
	if (0 > mcReq.get()) {
		mcReq.set( 0);
	}
	if (INSTANCE.isIdle()) {
//		INSTANCE.yield4Sync( 0);
		// Final refresh
//		Dib2Root.ui.feederCurrent.get().prepareFeed(INSTANCE.terminalMode);
		MainThreads.INSTANCE.prepareFeed();
		Dib2Root.platform.invalidate();
	}
}

private boolean trigger( long... parameters) {
	mExec.prepare( parameters);
	int c0 = mcReq.getAndIncrement();
	if (0 < c0) {
		return false;
	}
	threadPool.execute( this);
	return true;
}

//=====
}

public boolean triggerSync() {
	StoRunner.wxSyncRequested = true;
	return zRunnables[0].trigger();
}

public boolean triggerSaveOrInitialLoad() {
	StoRunner.wxSaveRequestedLoad = true;
	return zRunnables[0].trigger();
}

public boolean triggerWk() {
	return zRunnables[1].trigger();
}

public boolean onCloseFrame( int msecMax) {
	triggerSaveOrInitialLoad();
	try {
		Thread.yield();
		for (; msecMax > 0; msecMax -= 100) {
			try {
				Thread.sleep( 100);
			} catch (InterruptedException e) {
				break;
			}
			if (!StoRunner.wxSaveRequestedLoad) {
				break;
			}
		}
	} catch (Exception e) {
	}
	wxExitRequest = false;
	return !StoRunner.wxSaveRequestedLoad;
}

public void prepareFeed() {
	Dib2Root.ui.feederCurrent.get().prepareFeed( Dib2Root.ui.iSlide, Dib2Root.ui.iSlideSupplement, terminalMode);
	Dib2Root.platform.invalidate();
}

public static boolean stopMouseRep() {
	MouseClickedRepeater.INSTANCE.dEnd = 0;
	MainThreads.INSTANCE.prepareFeed();
	return true;
}

public static boolean triggerMouseRep( int xXPt10, int xYPt10) {
	QValToken cmd = UiPres.INSTANCE.getUiEventMouse( xXPt10, xYPt10);
	if (null != cmd) {
		cmd = Dib2Root.ui.feederCurrent.get().filter( cmd);
	}
	if (null == cmd) {
		stopMouseRep();
		return false;
	}
	if ((null == cmd.operator) && (0 < cmd.uiKeyOrButton)) {
		return zRunnables[2].trigger( (long) cmd.uiKeyOrButton);
	}
	stopMouseRep();
	if (null == cmd.operator) {
		return false;
	} else if ((QCalc.NOP == cmd.operator) && (0 < cmd.uiKeyOrButton)) {
		cmd = MouseClickedRepeater.INSTANCE.handleAsClick( cmd.uiKeyOrButton);
		if ((null == cmd) || (null == cmd.operator)) {
			return false;
		}
	}
	WkRunner.INSTANCE.wxGateIn.push( cmd);
	INSTANCE.triggerWk();
	return false;
}

/////

public boolean init( boolean terminalMode) {
	this.terminalMode = terminalMode;
	return true;
}

public boolean start() {
	Dib2Root.ui.feederMain = FeederRf.findFeeder( Dib2Root.appShort);
	Dib2Root.ui.feederCurrent = Dib2Root.ui.feederMain.get().switchFeeder( 0);
	return true;
}

private boolean isIdleOrPaused() {
	for (ExecWrapper ex : zRunnables) {
		if ((0 < ex.mcReq.get()) && !ex.mPaused) {
			return false;
		}
	}
	return true;
}

public boolean isIdle() {
	return isIdleOrPaused() && !zPauseRequest;
}

/** For StoRunner only. */
public boolean pauseAllOrRelease4StoRunner( boolean release, boolean force) {
	if (release) {
		zPauseRequest = false;
		return true;
	}
	if (!zPauseRequest) {
		zPauseRequest = true;
		return false;
	}
	return isIdleOrPaused();
}

//=====
}
