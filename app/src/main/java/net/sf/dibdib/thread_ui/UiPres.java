// Copyright (C) 2019,2020 Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_ui;

//import static net.sf.dibdib.thread_any.QCalc.*;
import static net.sf.dibdib.thread_any.StringFunc.*;
import static net.sf.dibdib.thread_x.UiDataSto.kBarTools;

import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.CcmTemplates.ColorNmz;
import net.sf.dibdib.generic.Feed.Script;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_wk.WkRunner;
import net.sf.dibdib.thread_x.*;

/** For platform's UI thread. */
public enum UiPres {
//=====

INSTANCE;

///// Threaded (in/ out/ trigger)

final QPlace wxGateIn = new QPlace();

// To be used by platform's UI: 'around' WkRunner.qUiView

public static volatile Script qUiBarTitle;
public static volatile Script qUiBarTools;
public static volatile Script qUiBarEntry;
public static volatile Script qUiBarStatus;
public static volatile Script qUiKeypad;
public static volatile int qUiKeypadInx = 1;

/** The text entry field, requires ziEntry to always stay in range. */
volatile String zEntry = "";
volatile int ziEntry = 0;

/////

//static UiFuncIf platformUi;
static int zUiKeypadLastInx = qUiKeypadInx;

int ziWidget = 0;

public boolean init() {
	WkRunner.INSTANCE.init( wxGateIn);
//	UiPres.platformUi = platformUi;
	final int displayX = Dib2Root.platform.getDisplayWidth();
	final int displayY = Dib2Root.platform.getDisplayHeight();
	UiDataSto.UI_DISPLAY_HEIGHT.prep( displayY, UiDataSto.MAIN);
	UiDataSto.UI_DISPLAY_WIDTH.prep( displayX, UiDataSto.MAIN);
	UiDataSto.MAIN = UiDataSto.freeze( null);
	return true;
}

public String setEntry( String entry) {
	String out = zEntry;
	ziEntry = 0;
	zEntry = entry;
	ziEntry = entry.length();
	return out;
}

public String getEntry( boolean raw) {
	return raw ? zEntry : StringFunc.string4Mnemonics( zEntry);
}

public QValToken startPushEvent( boolean execStep) {
	QValToken commandData = new QValToken();
	// ...
	if ((0 >= zEntry.length()) || !execStep) {
		commandData.operator = execStep ? QCalc.STEP : QCalc.NOP;
	} else {
		commandData.operator = QCalc.getOperator( zEntry);
		if (null != commandData.operator) {
			// Index first:
			ziEntry = 0;
			zEntry = "";
		}
	}
	String txt = zEntry;
	ziEntry = 0;
	zEntry = "";
	if (null != txt) {
		// No list processing yet
		txt = StringFunc.makePrintable( txt);
	}
	if ((null != txt) && (1 < txt.length())
		&& (('@' == txt.charAt( 0)) || (':' == txt.charAt( 0)))) {
		commandData.operator = ('@' == txt.charAt( 0)) ? QCalc.LOAD : QCalc.STORE;
		txt = txt.substring( 1);
	} else if (null != txt) {
		txt = StringFunc.string4Mnemonics( txt);
	}
	commandData.uiParameter = txt;
	return commandData;
}

/** Get command or key for mouse click.
 * @param xX UI's x
 * @param xY UI's y
 * @return command or key for command (with NOP) or key for entry.
 */
public QValToken getUiEventMouse( int xX, int xY) {
	QValToken commandData = new QValToken();
	commandData.operator = null;
	commandData.uiKeyOrButton = 0;
	final int bar = UiDataSto.UI_DISPLAY_BAR_HEIGHT.i32( null);
	final int sbar = UiDataSto.UI_DISPLAY_HEIGHT.i32( null) - bar;
	if (((Dib2Constants.UI_FRAME_BARS - 1) * bar) > xY) {
		if (bar > xY) {
			// Title bar.
			final int jX = Dib2Constants.UI_DSPL_INIT_MARGIN;
			final int e0 = UiDataSto.UI_LINE_SPACING_PT10.i32( null);
			final int dsp = UiDataSto.UI_DISPLAY_WIDTH.i32( null);
			final int inx = ((dsp >> 1) > xX)
				? ((xX - jX) / e0)
				: (-1 - ((dsp - jX - xX) / e0));
			if ((-(UiDataSto.kBarTitle.length >> 2) > inx) || (inx >= (UiDataSto.kBarTitle.length >> 2))) {
				return null;
			}
			char key = (char) UiDataSto.kBarTitle[1 + ((0 <= inx) ? (inx << 1) : (UiDataSto.kBarTitle.length + inx + inx))];
			commandData.operator = QCalc.NOP;
			commandData.uiKeyOrButton = (key < ' ') ? key
				//TODO
				: 0;
			return commandData;
		} else if ((2 * bar) >= xY) {
			// Tool bar.
			final int jX = Dib2Constants.UI_DSPL_INIT_MARGIN;
			final int e0 = UiDataSto.UI_DISPLAY_BAR_HEIGHT.i32( null);
			final int inx = (xX - jX) / e0;
			if ((UiDataSto.kBarTools.length <= inx) || (0 > inx)) {
				return null;
			}
			if (1 >= inx) {
				commandData.operator = (0 != inx) ? QCalc.VIEW : QCalc.LANG;
				commandData.uiParameter = "-1";
				return commandData;
			}
			commandData.operator = QCalc.getOperator( UiDataSto.kBarTools[inx].trim());
			if (null != commandData.operator) {
				commandData.uiParameter = (QCalc.VIEW == commandData.operator) ? "1" : zEntry;
				ziEntry = 0;
				zEntry = "";
			}
		} else {
			// Entry bar.
			if (((UiDataSto.UI_DISPLAY_WIDTH.i32( null) * 7) >> 3) < xX) {
				if (0 < zEntry.length()) {
					return startPushEvent( false);
				} else {
					return handleUiEventCtrl4UiThread( PUSH);
				}
			}
			return null;
		}
	} else if (sbar < xY) {
		// Status bar.
		if (((UiDataSto.UI_DISPLAY_WIDTH.i32( null) * 7) >> 3) < xX) {
			if (0 < zEntry.length()) {
				return startPushEvent( false);
			} else {
				return handleUiEventCtrl4UiThread( PUSH);
			}
		}
		//TODO
		return null;
	} else {
		// Canvas/ keypad.
		int inx = (((xY - (Dib2Constants.UI_FRAME_BARS - 1) * bar) * UiDataSto.qcKeys4Win) << 3)
			/ (sbar - (Dib2Constants.UI_FRAME_BARS - 1) * bar);
		int inx2 = ((xX * UiDataSto.qcKeys4Win) << 3) / UiDataSto.UI_DISPLAY_WIDTH.i32( null);
		if ((((inx + 1) % 8) <= 2) || (((inx2 + 1) % 8) <= 2)) {
			// Too far off.
			return null;
		}
		inx = (inx >> 3);
		inx2 = (inx2 >> 3);
		if ((0 > inx) || (inx >= UiDataSto.qcKeys4Win) || (0 > inx2) || (inx2 >= UiDataSto.qcKeys4Win)) {
			return null;
		}
		inx = inx * UiDataSto.qcKeys4Win + inx2;
		char k0 = UiDataSto.qPadKeys[qUiKeypadInx][inx];
		commandData.uiKeyOrButton = k0;
		if ((0 >= zEntry.length()) && (1 == qUiKeypadInx)) {
			// ...calc
			commandData.operator = QCalc.getOperator( "" + k0);
		} else if (qUiKeypadInx >= (UiDataSto.qPadKeys.length - 1)) {
			qUiKeypadInx = zUiKeypadLastInx;
			if (qUiKeypadInx >= (UiDataSto.qPadKeys.length - 1)) {
				qUiKeypadInx = 2;
			}
		}
	}
	return commandData;
}

public QValToken handleUiEventChar( char key) {
	final int iEntry = ziEntry;
	final String entry = zEntry;
	///// Atomic
	ziEntry = ((0 <= iEntry) && (iEntry <= entry.length())) ? iEntry : entry.length();
	if (' ' <= key) {
		zEntry = entry.substring( 0, iEntry) + key + entry.substring( iEntry);
		ziEntry = iEntry + 1;
		ziWidget = 0;
	}
	MainThreads.INSTANCE.prepareFeed();
	return null;
}

private QValToken handleUiEventCtrl( char key) {
	QValToken commandData = new QValToken();
	commandData.operator = null;
	commandData.uiKeyOrButton = key;
	final int iWidget = ziWidget;
	ziWidget = 0;
	int i0 = ziEntry;
	Dib2Root.ui.error = null;
	String s0 = null;
	switch (key) {

	//// Special cases first ...

	case STEP:
		commandData.operator = QCalc.getOperator( zEntry);
		commandData = Dib2Root.ui.feederCurrent.get().filter( commandData);
		if ((null == commandData) || (null != commandData.operator)) {
			ziEntry = 0;
			zEntry = "";
			return commandData;
		}
		// Fall through.
	case CR:
	case LF:
	case PUSH:
	case XCOPY:
	case XCUT:
		// Keep volatile index in range:
		ziEntry = 0;
//		if (XCOPY != key) {
		commandData = startPushEvent( 0 != iWidget);
//		} else {
//			commandData.uiParameter = zEntry;
		commandData = Dib2Root.ui.feederCurrent.get().filter( commandData);
		if (null == commandData) {
			return null;
		}

		s0 = commandData.uiParameter;
		commandData.uiParameter = (QCalc.NOP != commandData.operator) ? null : s0;
		final boolean fill = (0 >= s0.length());
		if (fill && ((0 == iWidget) || ((CR != key) && (LF != key)))) {
			QValToken arg = CcmSto.instance.stackPeek( 0);
			if ((null != arg) && (null != arg.val) && (0 < arg.val.length)) {
				zEntry = QValMapSto.string4QVals( arg.val);
				if (XCUT == key) {
					commandData.operator = QCalc.CLEAR;
				} else if (STEP == key) {
					zEntry = "";
					commandData.operator = QCalc.DUP;
				} else {
					// Finished.
					commandData = null;
				}
			}
		} else if (XCOPY == key) {
			zEntry = s0;
			// Finished.
			commandData = null;
		}
		if ((XCUT == key) || (XCOPY == key)) {
			Dib2Root.platform.pushClipboard( "edit", fill ? zEntry : s0);
		}
		ziEntry = zEntry.length();
		return commandData;

	case PSHIFT:
		char sel = (0 >= ziEntry) ? ' ' : zEntry.charAt( ziEntry - 1);
		setUnicodeSelection( sel);
		// Fall through
	case BACKSP:
		ziEntry = ((0 < ziEntry) && (ziEntry < zEntry.length())) ? ziEntry : zEntry.length();
		if (0 < ziEntry) {
			zEntry = zEntry.substring( 0, ziEntry - 1) + zEntry.substring( ziEntry);
			--ziEntry;
		}
		return null;

	case ALT:
	case SHIFT:
		if ((UiDataSto.qPadKeys.length - 1) <= qUiKeypadInx) {
			if ((UiDataSto.keys_UniBlock_Current <= UiDataSto.keys_UniBlock_Offset) && (ALT == key)) {
				qUiKeypadInx = UiDataSto.keys_UniBlock_FromPad;
				return null;
			}
			UiDataSto.setUnicodeBlock( -1, (ALT == key) ? -1 : 1);
			return null;
		}
		//		switchBackOrCircle( 0, SWI_KEYPAD_INDEX, (key != ALT));
		qUiKeypadInx += (ALT != key) ? 1 : ((0 < qUiKeypadInx) ? -1 : 0);
		return null;
	case ESCAPE:
		UiDataSto.reset( null);
		commandData.operator = QCalc.ESCAPE;
		return commandData;

	/////

	case MOVE_LEFT:
		// ...mode
		ziEntry = ((0 < i0) && (i0 <= zEntry.length())) ? (i0 - 1) : 0;
		return null;
	case MOVE_RIGHT:
		ziEntry = ((i0 + 1) < zEntry.length()) ? (i0 + 1) : (zEntry.length() - 1);
		return null;
	case SCROLL_DOWN: {
		final int offs = UiDataSto.UI_DISPLAY_OFFS_CANVAS_Y.i32( null);
		final int dsp = UiDataSto.UI_DISPLAY_HEIGHT.i32( null)
			- Dib2Constants.UI_FRAME_BARS * UiDataSto.UI_DISPLAY_BAR_HEIGHT.i32( null);
		final int split = UiDataSto.UI_DISPLAY_SPLIT_CANVAS_Y.i32( null);
		final int gap = UiDataSto.UI_DISPLAY_SPLIT_CANVAS_GAP_Y.i32( null);
		final int zoom = UiDataSto.UI_ZOOMLVL_CANVAS.i32( null);
		final int board = UiDataSto.UI_BOARD_HEIGHT.i32( null);
		int delta = dsp >> ((0 < zoom) ? zoom : 1);
		if (((offs + gap + dsp) < board) && (split <= ((dsp >> 1) + 1))) {
			if ((0 >= offs) && ((Dib2Constants.UI_DSPL_NMZ_LF * 2) > gap)) {
				UiDataSto.UI_DISPLAY_SPLIT_CANVAS_GAP_Y.prep( Dib2Constants.UI_DSPL_NMZ_LF * 2, null);
			} else {
				UiDataSto.UI_DISPLAY_SPLIT_CANVAS_GAP_Y.prep( gap + delta, null);
			}
		} else if (split <= ((dsp >> 1) + 1)) {
			UiDataSto.UI_DISPLAY_SPLIT_CANVAS_Y.prep( dsp - split, null);
		} else if (0 < gap) {
			delta = ((delta > split) && (split > 0)) ? split : delta;
			delta = (gap >= delta) ? delta : gap;
			UiDataSto.UI_DISPLAY_SPLIT_CANVAS_GAP_Y.prep( gap - delta, null);
			UiDataSto.UI_DISPLAY_OFFS_CANVAS_Y.prep( offs + delta, null);
		} else {
			delta = ((offs + delta) < board) ? delta : (board - delta - offs);
			UiDataSto.UI_DISPLAY_OFFS_CANVAS_Y.prep( offs + delta, null);
		}
	}
		break;
	case SCROLL_LEFT: {
		final int offs = UiDataSto.UI_DISPLAY_OFFS_CANVAS_X.i32( null);
		final int dsp = UiDataSto.UI_DISPLAY_WIDTH.i32( null);
		final int split = UiDataSto.UI_DISPLAY_SPLIT_CANVAS_X.i32( null);
		final int gap = UiDataSto.UI_DISPLAY_SPLIT_CANVAS_GAP_X.i32( null);
		final int zoom = UiDataSto.UI_ZOOMLVL_CANVAS.i32( null);
		int delta = dsp >> ((0 < zoom) ? zoom : 1);
		if ((0 < gap) && (split <= ((dsp >> 1) + 1))) {
			delta = (delta <= gap) ? delta : gap;
			UiDataSto.UI_DISPLAY_SPLIT_CANVAS_GAP_X.prep( gap - delta, null);
		} else if (split <= ((dsp >> 1) + 1)) {
			UiDataSto.UI_DISPLAY_SPLIT_CANVAS_X.prep( dsp - split, null);
		} else if (0 < offs) {
			delta = ((delta > split) && (split > 0)) ? split : delta;
			delta = (delta <= offs) ? delta : offs;
			UiDataSto.UI_DISPLAY_OFFS_CANVAS_X.prep( offs - delta, null);
			UiDataSto.UI_DISPLAY_SPLIT_CANVAS_GAP_X.prep( gap + delta, null);
		} else {
			UiDataSto.UI_DISPLAY_SPLIT_CANVAS_X.prep( dsp - split, null);
		}
	}
		break;
	case SCROLL_RIGHT: {
		final int offs = UiDataSto.UI_DISPLAY_OFFS_CANVAS_X.i32( null);
		final int dsp = UiDataSto.UI_DISPLAY_WIDTH.i32( null);
		final int split = UiDataSto.UI_DISPLAY_SPLIT_CANVAS_X.i32( null);
		final int gap = UiDataSto.UI_DISPLAY_SPLIT_CANVAS_GAP_X.i32( null);
		final int zoom = UiDataSto.UI_ZOOMLVL_CANVAS.i32( null);
		final int board = UiDataSto.UI_BOARD_WIDTH.i32( null);
		int delta = dsp >> ((0 < zoom) ? zoom : 1);
		if (((offs + gap + dsp) < board) && (split <= ((dsp >> 1) + 1))) {
			if ((0 >= offs) && ((Dib2Constants.UI_DSPL_NMZ_TAB / 2) > gap)) {
				UiDataSto.UI_DISPLAY_SPLIT_CANVAS_GAP_X.prep( Dib2Constants.UI_DSPL_NMZ_TAB / 2, null);
			} else {
				UiDataSto.UI_DISPLAY_SPLIT_CANVAS_GAP_X.prep( gap + delta, null);
			}
		} else if (split <= ((dsp >> 1) + 1)) {
			UiDataSto.UI_DISPLAY_SPLIT_CANVAS_X.prep( dsp - split, null);
		} else if (0 < gap) {
			delta = ((delta > split) && (split > 0)) ? split : delta;
			delta = (gap >= delta) ? delta : gap;
			UiDataSto.UI_DISPLAY_SPLIT_CANVAS_GAP_X.prep( gap - delta, null);
			UiDataSto.UI_DISPLAY_OFFS_CANVAS_X.prep( offs + delta, null);
		} else {
			delta = ((offs + delta) < board) ? delta : (board - delta - offs);
			UiDataSto.UI_DISPLAY_OFFS_CANVAS_X.prep( offs + delta, null);
		}
	}
		break;
	case SCROLL_UP: {
		final int offs = UiDataSto.UI_DISPLAY_OFFS_CANVAS_Y.i32( null);
		final int dsp = UiDataSto.UI_DISPLAY_HEIGHT.i32( null)
			- Dib2Constants.UI_FRAME_BARS * UiDataSto.UI_DISPLAY_BAR_HEIGHT.i32( null);
		final int split = UiDataSto.UI_DISPLAY_SPLIT_CANVAS_Y.i32( null);
		final int gap = UiDataSto.UI_DISPLAY_SPLIT_CANVAS_GAP_Y.i32( null);
		final int zoom = UiDataSto.UI_ZOOMLVL_CANVAS.i32( null);
		int delta = dsp >> ((0 < zoom) ? zoom : 1);
		if ((0 < gap) && (split <= ((dsp >> 1) + 1))) {
			delta = (delta <= gap) ? delta : gap;
			UiDataSto.UI_DISPLAY_SPLIT_CANVAS_GAP_Y.prep( gap - delta, null);
		} else if (split <= ((dsp >> 1) + 1)) {
			UiDataSto.UI_DISPLAY_SPLIT_CANVAS_Y.prep( dsp - split, null);
		} else if (0 < offs) {
			delta = ((delta > split) && (split > 0)) ? split : delta;
			delta = (delta <= offs) ? delta : offs;
			UiDataSto.UI_DISPLAY_OFFS_CANVAS_Y.prep( offs - delta, null);
			UiDataSto.UI_DISPLAY_SPLIT_CANVAS_GAP_Y.prep( gap + delta, null);
		} else {
			UiDataSto.UI_DISPLAY_SPLIT_CANVAS_Y.prep( dsp - split, null);
		}
	}
		break;
	case TAB:
		// ...
		ziWidget = (0 == iWidget) ? -1 : 0;
		return null;
	case XPASTE:
		s0 = Dib2Root.platform.getClipboardText();
		if (null != s0) {
			final String entry = zEntry;
			final int ix = ziEntry;
			ziEntry = 0;
			if (ix <= entry.length()) {
				zEntry = entry.substring( 0, ix) + s0 + entry.substring( ix);
			}
			ziEntry = zEntry.length();
			Dib2Root.platform.invalidate();
		}
		return null;
	case ZOOM_IN:
	case ZOOM_OUT:
		i0 = UiDataSto.UI_ZOOMLVL_CANVAS.i32( null);
		UiDataSto.UI_ZOOMLVL_CANVAS.prep( i0 + ((ZOOM_IN == key) ? 1 : -1), null);
		if ((ZOOM_OUT == key) && (-2 >= i0)) {
			if (UiDataSto.UI_DISPLAY_SPLIT_CANVAS_GAP_X.i32( null) < (UiDataSto.UI_DISPLAY_WIDTH.i32( null) >> 2)) {
				UiDataSto.UI_DISPLAY_SPLIT_CANVAS_GAP_X.prep( 0, null);
			}
			if (UiDataSto.UI_DISPLAY_SPLIT_CANVAS_GAP_Y.i32( null) < (UiDataSto.UI_DISPLAY_HEIGHT.i32( null) >> 2)) {
				UiDataSto.UI_DISPLAY_SPLIT_CANVAS_GAP_Y.prep( 0, null);
			}
		}
		return null;
	default:
		;
	}
	return null;

}

public QValToken handleUiEventCtrl4UiThread( char key) {
	QValToken cmd = handleUiEventCtrl( key);
	MainThreads.INSTANCE.prepareFeed();
	return (null == cmd) ? cmd : Dib2Root.ui.feederCurrent.get().filter( cmd);
}

private Feed.Script[] barTitle_script = new Feed.Script[30];

private Feed.Script barTitle() {
	int no = 0;
	int count = 0;
	final int need = 1 + UiDataSto.kBarTitle.length * 3 / 2;
	if (need > barTitle_script.length) {
		barTitle_script = new Feed.Script[need];
	}
	final int e0 = UiDataSto.UI_LINE_SPACING_PT10.i32( null);
	int posX = UiDataSto.UI_DISPLAY_WIDTH.i32( null) - Dib2Constants.UI_DSPL_INIT_MARGIN;
	final int base = (UiDataSto.UI_DISPLAY_BAR_HEIGHT.i32( null)
		+ ((UiDataSto.UI_FONT_SIZE_FRAME_PT10.i32( null)
			* Dib2Constants.UI_FONT_NMZ_CAP_H) >> Dib2Constants.UI_FONT_NMZ_SHIFT)) >> 1;
	int step = -2;
	barTitle_script[count++] = Feed.makeScriptEl( ++no, Feed.RBASE, base);
	for (int i0 = UiDataSto.kBarTitle.length - 2; (i0 != (2 * Dib2Constants.UI_FRAME_BAR_ITEMS_PER_SIDE))
		|| (0 > step); i0 += step) {
		barTitle_script[count++] = Feed.makeScriptEl( ++no, Feed.POS, posX, 0);
		posX += (0 > step) ? (-e0) : e0;
		barTitle_script[count++] = Feed.makeScriptEl( ++no, Feed.RGBCOLOR, UiDataSto.kBarTitle[i0]);
		char ch = (char) UiDataSto.kBarTitle[i0 + 1];
		barTitle_script[count++] = Feed.makeScriptEl( ++no, (0 > step) ? Feed.TXSHLEFT : Feed.TEXT,
			"" + ((' ' <= ch) ? ch : StringFunc.kControlAsButton[ch]));
		if (i0 == (2 * Dib2Constants.UI_FRAME_BAR_ITEMS_PER_SIDE)) {
			step = 2;
			posX = Dib2Constants.UI_DSPL_INIT_MARGIN;
			i0 = -2;
		}
	}
	Feed.Script out = Feed.makeScript( 0);
	out.script = barTitle_script;
	out.cScript = count;
	return out;
}

private Feed.Script[] barTools_script = new Feed.Script[50];

private Feed.Script barTools() {
	int no = 0;
	int count = 0;
	final int need = 3 + 2 * 2 * UiDataSto.kBarTools.length;
	if (need > barTools_script.length) {
		barTools_script = new Feed.Script[need];
	}
	int posX = Dib2Constants.UI_DSPL_INIT_MARGIN;
	final int e0 = UiDataSto.UI_DISPLAY_BAR_HEIGHT.i32( null);
	final int linesp = (3 * e0) >> 3;
	barTools_script[count++] = Feed.makeScriptEl( ++no, Feed.FACE, 0);
	barTools_script[count++] = Feed.makeScriptEl( ++no, Feed.HEIGHT, linesp);
	final int base = (e0 - (linesp - ((linesp * Dib2Constants.UI_FONT_NMZ_CAP_H) >> Dib2Constants.UI_FONT_NMZ_SHIFT))) >> 1;
	barTools_script[count++] = Feed.makeScriptEl( ++no, Feed.RBASE, base);
	for (int i0 = 0; i0 < UiDataSto.kBarTools.length; ++i0, posX += e0) {
		String part0 = kBarTools[i0].substring( 0, 2);
		String part1 = kBarTools[i0].substring( 2);
		if ((0 > ziWidget) && "GO".equals( part1)) {
			// ...
			part0 = "**";
		} else if ("VW".equals( part0)) {
			part1 = Dib2Root.ui.feederCurrent.getShortId2();
		}
		barTools_script[count++] = Feed.makeScriptEl( ++no, Feed.POS, posX, 0);
		barTools_script[count++] = Feed.makeScriptEl( ++no, Feed.TEXT, part0);
		barTools_script[count++] = Feed.makeScriptEl( ++no, Feed.POS, posX, linesp);
		barTools_script[count++] = Feed.makeScriptEl( ++no, Feed.TEXT, part1);
	}
	Feed.Script out = Feed.makeScript( 0);
	out.script = barTools_script;
	out.cScript = count;
	return out;
}

private Feed.Script[] barEntry_script = new Feed.Script[10];

private Feed.Script barEntry() {
	int no = 0;
	int count = 0;
	// Atomic!
	int iEntry = ziEntry;
	String entry = zEntry;
	iEntry = (0 <= iEntry) && (iEntry <= entry.length()) ? iEntry : entry.length();
	final int e0 = (5 * UiDataSto.UI_DISPLAY_WIDTH.i32( null)) >> 3;
	final String part0 = entry.substring( 0, iEntry);
	final String part1 = entry.substring( iEntry);
	final int posX = UiDataSto.UI_DISPLAY_WIDTH.i32( null) - Dib2Constants.UI_DSPL_INIT_MARGIN;
	int base = (UiDataSto.UI_DISPLAY_BAR_HEIGHT.i32( null)
		+ ((UiDataSto.UI_FONT_SIZE_FRAME_PT10.i32( null)
			* Dib2Constants.UI_FONT_NMZ_X_HEIGHT) >> Dib2Constants.UI_FONT_NMZ_SHIFT)) >> 1;
	barEntry_script[count++] = Feed.makeScriptEl( ++no, Feed.POS, e0, 0);
	barEntry_script[count++] = Feed.makeScriptEl( ++no, Feed.LINE, e0, UiDataSto.UI_DISPLAY_BAR_HEIGHT.i32( null));
	barEntry_script[count++] = Feed.makeScriptEl( ++no, Feed.POS, e0 -
		(part0.endsWith( " ") ? (Dib2Constants.UI_DSPL_INIT_MARGIN << 1) : Dib2Constants.UI_DSPL_INIT_MARGIN), 0);
	barEntry_script[count++] = Feed.makeScriptEl( ++no, Feed.RBASE, base);
	barEntry_script[count++] = Feed.makeScriptEl( ++no, Feed.TXSHLEFT, part0);
	barEntry_script[count++] = Feed.makeScriptEl( ++no, Feed.POS, e0 + Dib2Constants.UI_DSPL_INIT_MARGIN, 0);
	barEntry_script[count++] = Feed.makeScriptEl( ++no, Feed.TEXT, part1);
	barEntry_script[count++] = Feed.makeScriptEl( ++no, Feed.POS, posX, 0);
	barEntry_script[count++] = Feed.makeScriptEl( ++no, Feed.RGBCOLOR, ColorNmz.GREEN.argb);
	barEntry_script[count++] = Feed.makeScriptEl( ++no, Feed.TXSHLEFT, ">");
	Feed.Script out = Feed.makeScript( 0);
	out.script = barEntry_script;
	out.cScript = count;
	return out;
}

private Feed.Script[] barStatus_script = new Feed.Script[13];

private Feed.Script barStatus() {
	int no = 0;
	int count = 0;
	final int height = (3 * UiDataSto.UI_DISPLAY_BAR_HEIGHT.i32( null) >> 3);
	final int posX = UiDataSto.UI_DISPLAY_WIDTH.i32( null) - Dib2Constants.UI_DSPL_INIT_MARGIN;
	final int base = (UiDataSto.UI_DISPLAY_BAR_HEIGHT.i32( null)
		+ ((height //UiDataSto.UI_FONT_SIZE_FRAME_PT10.i32( null)
			* Dib2Constants.UI_FONT_NMZ_CAP_H) >> Dib2Constants.UI_FONT_NMZ_SHIFT)) >> 1;
	barStatus_script[count++] = Feed.makeScriptEl( ++no, Feed.POS, 0, 0);
	barStatus_script[count++] = Feed.makeScriptEl( ++no, Feed.RBASE, base);
	barStatus_script[count++] = Feed.makeScriptEl( ++no, Feed.HEIGHT,
		height);//UiDataSto.UI_DISPLAY_BAR_HEIGHT.i32( null) >> 1);
	String progress = (null == Dib2Root.ui.error) ? Dib2Root.ui.progress : Dib2Root.ui.error;
	int prcolor = ColorNmz.ORANGE_DUSTY.argb;
	if (MainThreads.INSTANCE.isIdle() && (null == Dib2Root.ui.error)) {
		Dib2Root.ui.progress = null;
		progress = "OK";
		prcolor = ColorNmz.GREEN.argb;
	}
	if (null != progress) {
		barStatus_script[count++] = Feed.makeScriptEl( ++no, Feed.POS, Dib2Root.UI_DSPL_INIT_MARGIN, 0);
		barStatus_script[count++] = Feed.makeScriptEl( ++no, Feed.RGBCOLOR, prcolor);
		barStatus_script[count++] = Feed.makeScriptEl( ++no, Feed.TEXT, progress);
	}
	barStatus_script[count++] = Feed.makeScriptEl( ++no, Feed.POS, UiDataSto.UI_DISPLAY_WIDTH.i32( null) >> 1, 0);
	barStatus_script[count++] = Feed.makeScriptEl( ++no, Feed.RGBCOLOR, ColorNmz.BLACK.argb);
	barStatus_script[count++] = Feed.makeScriptEl( ++no, Feed.TXCTR, "< " + Dib2Root.ui.iSlide + " >");
	barStatus_script[count++] = Feed.makeScriptEl( ++no, Feed.POS, posX, 0);
//	barStatus_script[count++] = Feed.makeScriptEl( ++no, Feed.HEIGHT, UiDataSto.UI_FONT_SIZE_FRAME_PT10.i32( null));
	barStatus_script[count++] = Feed.makeScriptEl( ++no, Feed.RGBCOLOR, ColorNmz.GREEN.argb);
	barStatus_script[count++] = Feed.makeScriptEl( ++no, Feed.TXSHLEFT, ">");
	Feed.Script out = Feed.makeScript( 0);
	out.script = barStatus_script;
	out.cScript = count;
	return out;
}

public Feed.Script keypad() {
	int no = 0;
	int count = 0;
	Feed.Script[] script = new Feed.Script[2 + 3 * UiDataSto.qcKeys4Win * UiDataSto.qcKeys4Win];
	final int ex = UiDataSto.UI_DISPLAY_WIDTH.i32( null) / UiDataSto.qcKeys4Win;
	final int linesp = (UiDataSto.UI_DISPLAY_HEIGHT.i32( null)
		- 4 * UiDataSto.UI_DISPLAY_BAR_HEIGHT.i32( null)) / UiDataSto.qcKeys4Win;
	//	script[count++] = QFeed.makeScriptEl( ++no, QFeed.FACE, 0);
	final int height = (3 * linesp) >> 2;
	script[count++] = Feed.makeScriptEl( ++no, Feed.HEIGHT, height);
	final int base = linesp - ((linesp -
		((height * Dib2Constants.UI_FONT_NMZ_CAP_H) >> Dib2Constants.UI_FONT_NMZ_SHIFT)) >> 1);
	script[count++] = Feed.makeScriptEl( ++no, Feed.RBASE, base);
	int nY = 0;
	for (int i1 = 0; i1 < UiDataSto.qcKeys4Win; ++i1) {
		int nX = (ex + UiDataSto.qcKeys4Win) >> 1;
		for (int i0 = 0; i0 < UiDataSto.qcKeys4Win; ++i0) {
			char tK = UiDataSto.qPadKeys[qUiKeypadInx][i1 * UiDataSto.qcKeys4Win + i0];
			script[count++] = Feed.makeScriptEl( ++no, Feed.POS, nX, nY);
			int color = ColorNmz.GREEN.argb & 0x42ffffff;
			if (' ' > tK) {
				tK = StringFunc.kControlAsButton[tK];
				color = ColorNmz.BLUEBLUE.argb & 0x42ffffff;
			}
			script[count++] = Feed.makeScriptEl( ++no, Feed.RGBCOLOR, color);
			script[count++] = Feed.makeScriptEl( ++no, Feed.TXCTR, "" + tK);
			nX += ex;
		}
		nY += linesp;
	}
	Feed.Script out = Feed.makeScript( 0);
	out.script = script;
	out.cScript = count;
	return out;
}

public static void setUnicodeSelection( char base) {
	UiDataSto.keys_UniBlock_Offset = UiDataSto.setUnicodeBlock( base, 0);
	UiDataSto.keys_UniBlock_FromPad = qUiKeypadInx;
	String group = StringFunc.group4Rfc1345( base);
	int count = 0;
	final int len = (group.length() > UiDataSto.keys_UnicodeSel.length) ? UiDataSto.keys_UnicodeSel.length : group.length();
	for (int i0 = 0; i0 < len; ++i0) {
		if (UiDataSto.kKeys_A_Dvorak[i0] <= ' ') {
			UiDataSto.keys_UnicodeSel[i0] = UiDataSto.kKeys_A_Dvorak[i0];
		} else {
			UiDataSto.keys_UnicodeSel[i0] = group.charAt( count++);
		}
	}
	zUiKeypadLastInx = qUiKeypadInx;
	qUiKeypadInx = UiDataSto.qPadKeys.length - 1;
}

/** Create title bar, status bar etc., can be called from any thread.
 */
public static synchronized void prepareUiFrameData() {
	if (MainThreads.INSTANCE.isIdle()) {
		UiDataSto.MAIN = UiDataSto.freeze( null);
	}
	// For recycling - to be done after atomic value replacements.
	Script[] oldTitles = new Script[] { qUiBarTitle, qUiBarTools, qUiBarEntry, qUiBarStatus, qUiKeypad };
	qUiBarTitle = INSTANCE.barTitle();
	qUiBarTools = INSTANCE.barTools();
	qUiBarEntry = INSTANCE.barEntry();
	qUiBarStatus = INSTANCE.barStatus();
	qUiKeypad = INSTANCE.keypad();
	for (Script scr : oldTitles) {
		if (null != scr) {
			scr.recycleMe();
		}
	}
}

//=====
}
