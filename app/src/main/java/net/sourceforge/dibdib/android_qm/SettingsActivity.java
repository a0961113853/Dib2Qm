// Copyright (C) 2018  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.android_qm;

import com.gitlab.dibdib.dib2qm.quickmsg_db;

public final class SettingsActivity extends net.vreeken.quickmsg.SettingsActivity_0 {
//=====

@Override
public void onPause() {
	quickmsg_db db = new quickmsg_db( this);
	db.save();
	super.onPause();
}

//=====
}
