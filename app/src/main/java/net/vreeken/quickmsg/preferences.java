// Copyright (C) 2016,2017,2018  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// This part is new code for working with QuickMSG.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.vreeken.quickmsg;

// import net.vreeken.quickmsg.*;

import com.gitlab.dibdib.dib2qm.*;
import com.gitlab.dibdib.joined_dib2qm.*;

public class preferences extends quickmsg_db {
//=====

public preferences( Object context ) {
	super( context );
//	if (null == Dib2Root.qContextQm) {
//	Dib2Root.init( 'A', "qm", qDbFileName, this );
	Pgp.privProvs = (null == Pgp.privProvs) //,
	? new PrivProvIf[] { new EcDhQm() /*net.vreeken.quickmsg.pgp*/} //,
		: Pgp.privProvs;
}

//=====
}
